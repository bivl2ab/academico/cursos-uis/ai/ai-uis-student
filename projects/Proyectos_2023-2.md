---
# Proyectos 2023-2. Inteligencia Artificial. 

## Prof: Fabio Martínez, Ph.D
## Prof: Gustavo Garzón
---

# Lista de Proyectos
1. [Aerolinea](#proy1)
2. [CarvasPredicter](#proy2)
3. [DETECCION DE LETRAS DEL ABECEDARIO DEL LENGUAJE DE SEÑAS COLOMBIANO (LSC)](#proy3)
4. [Deteccion mortalidad de enfermedades cardiacas](#proy4)
5. [Handwritten Alphabet Letter Recognition System](#proy5)
6. [DEC: Diagnóstico de Enmerfedad Cardiovascular](#proy6)
7. [Dataset precio BITCOIN](#proy7)
8. [Red de calidad de aire - datos metereológicos](#proy8)
9. [Clasificador de riesgo de cáncer de pulmón basado en los síntomas del paciente](#proy9)
10. [Sistema de Predicción Temprana de Obesidad](#proy10)
11. [Royal Flush: Classification models for a Poker Hands Dataset](#proy11)
12. [PD AI](#proy12)
13. [Clasificación de posiciones finales de pilotos en la Fórmula 1](#proy13)
14. [El ojo de bucar](#proy14)
15. [Predicción economía y precio de la BigMac](#proy15)
16. [Estimación precios del Bitcoin](#proy16)
17. [SP-MUSIC_AI](#proy17)
18. [Diseño de lentes de contacto](#proy18)
19. [Clasificador de aplicaciones Play Store](#proy19)
20. [Olimpics Winner Predictor](#proy20)
21. [Medicamentos Vigentes](#proy21)
22. [Detección de anomalías en trenes](#proy22)
23. [Clasificación de cuentas falsas de Instagram con IA](#proy23)
24. [Pronóstico de aceptación de depósito a plazo](#proy24)
25. [Análisis de Riesgo Crediticio](#proy25)


---

## Aerolinea <a name="proy1"></a>

**Autores: Oscar Julian Rey Jaimes - 2202032, Yonathan Camilo Benítez Mancipe - 2204133, Juan Pablo Arias Remolina - 2204251**

<img src="https://private-user-images.githubusercontent.com/130578363/288527801-931f7cda-7e6d-4d58-9471-534036a3254a.jpeg?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MDU0MzgxODgsIm5iZiI6MTcwNTQzNzg4OCwicGF0aCI6Ii8xMzA1NzgzNjMvMjg4NTI3ODAxLTkzMWY3Y2RhLTdlNmQtNGQ1OC05NDcxLTUzNDAzNmEzMjU0YS5qcGVnP1gtQW16LUFsZ29yaXRobT1BV1M0LUhNQUMtU0hBMjU2JlgtQW16LUNyZWRlbnRpYWw9QUtJQVZDT0RZTFNBNTNQUUs0WkElMkYyMDI0MDExNiUyRnVzLWVhc3QtMSUyRnMzJTJGYXdzNF9yZXF1ZXN0JlgtQW16LURhdGU9MjAyNDAxMTZUMjA0NDQ4WiZYLUFtei1FeHBpcmVzPTMwMCZYLUFtei1TaWduYXR1cmU9MTI3MjgxOTJkNjQ5MDcxOTBkYWRiMmRmYjdjMzhkYWExMjg2MzJjNjliYTdjOGE3ODc4NmM5OGNjZTBkY2QyNCZYLUFtei1TaWduZWRIZWFkZXJzPWhvc3QmYWN0b3JfaWQ9MCZrZXlfaWQ9MCZyZXBvX2lkPTAifQ.7ClH_Lcj_ga_TpvWWuKhEqKduKPUyS7-6FHfuk0y0o4" style="width:700px;">

****

- Modelo: desarrollar un modelo predictivo avanzado mediante técnicas de Inteligencia Artificial (IA), con la capacidad de anticipar y clasificar el estado de un vuelo como 'Delayed' (retrasado), 'On Time' (a tiempo) o 'Cancelled' (cancelado)
- Video: https://www.youtube.com/watch?v=_-RJ69_ygxU

[(project sources)](https://github.com/Camilo802/Airline)

---
## CarvasPredicter <a name="proy2"></a>

**Autores: Valentina Álvarez, Luis Lemus, Andrés Olivar y Juan Sepúlveda.**

<img src="https://raw.githubusercontent.com/JuanSepu18/CarvasPredicter/main/banner_proyecto.png" style="width:700px;">

****

- Modelo: Predecir con la mayor exactitud posible el riesgo de enfermedad cardiaca con datos proporcionados por el paciente.
- Video: https://youtu.be/ldP-keRNpOM?si=eL-NhM_5QhSszoz3

[(project sources)](https://github.com/JuanSepu18/CarvasPredicter)

---
## DETECCION DE LETRAS DEL ABECEDARIO DEL LENGUAJE DE SEÑAS COLOMBIANO (LSC) <a name="proy3"></a>

**Autores: Andrés Felipe Mejía Perdomo, Juan Camilo Garavito Lozada, Jerson Julian Cañon Castillo.**

<img src="https://raw.githubusercontent.com/afxmejia02/DetectorLSC-ProyectoIA/main/multimedia/Banner.png" style="width:700px;">

****

- Modelo: Predecir la letra del abecedario en lenguaje de señas colombiano segun la imagen para asi ayudar a la comunicacion de las personas sordas en el pais
- Video: https://www.youtube.com/watch?v=AX7lqDK1lRM

[(project sources)](https://github.com/afxmejia02/DetectorLSC-ProyectoIA)

---
## Deteccion mortalidad de enfermedades cardiacas <a name="proy4"></a>

**Autores: Javier Andrés Peña Vargas - 2204123 , Santiago Yarce Prince - 2204122.**

<img src="https://private-user-images.githubusercontent.com/143100480/287475111-a0818a45-81dc-437f-954a-ab93c75235f6.jpeg?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MDU0Mzc5OTksIm5iZiI6MTcwNTQzNzY5OSwicGF0aCI6Ii8xNDMxMDA0ODAvMjg3NDc1MTExLWEwODE4YTQ1LTgxZGMtNDM3Zi05NTRhLWFiOTNjNzUyMzVmNi5qcGVnP1gtQW16LUFsZ29yaXRobT1BV1M0LUhNQUMtU0hBMjU2JlgtQW16LUNyZWRlbnRpYWw9QUtJQVZDT0RZTFNBNTNQUUs0WkElMkYyMDI0MDExNiUyRnVzLWVhc3QtMSUyRnMzJTJGYXdzNF9yZXF1ZXN0JlgtQW16LURhdGU9MjAyNDAxMTZUMjA0MTM5WiZYLUFtei1FeHBpcmVzPTMwMCZYLUFtei1TaWduYXR1cmU9MTYyZDk3YjdkMTAyOGU3ODA5YjBjOTU1M2QyNjgyMjgxMDgzMmJmZWFjZTNhMDIzOTk1YmQxMTZjMDNhOGUxZiZYLUFtei1TaWduZWRIZWFkZXJzPWhvc3QmYWN0b3JfaWQ9MCZrZXlfaWQ9MCZyZXBvX2lkPTAifQ.p3HiEbcO7TXFmcfTil2XOemZQSyV4KmPnh2ClgziGuk" style="width:700px;">

****

- Modelo: Poder disminuir victimas de enfermedades cardiacas
- Video: https://www.youtube.com/watch?v=4KmTj00PkQ0

[(project sources)](https://github.com/javierPena28/Proyecto-IA/blob/main/README.md)

---
## Handwritten Alphabet Letter Recognition System <a name="proy5"></a>

**Autores: Carlos Daniel Peñaloza Torres**

<img src="https://raw.githubusercontent.com/Pholluxion/potential-system/main/assets/cover.png" style="width:700px;">

****

- Modelo: Desarrollar e implementar un sistema de reconocimiento de escritura a mano empleando algoritmos de inteligencia artificial.
- Video: https://github.com/Pholluxion/potential-system/blob/main/link

[(project sources)](https://github.com/Pholluxion/potential-system/tree/main)

---

## DEC: Diagnóstico de Enmerfedad Cardiovascular <a name="proy6"></a>

**Autores: Johan Sebastián León Peñaloza - 2202037, Marcos Duván Nítola Rodríguez - 2200146, Santiago González Flores - 2200165**

<img src="https://private-user-images.githubusercontent.com/99308344/289706129-5e7eb1d3-77cd-4bd0-a1b7-c1abee9d414a.png?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MDU0Mzc1MDgsIm5iZiI6MTcwNTQzNzIwOCwicGF0aCI6Ii85OTMwODM0NC8yODk3MDYxMjktNWU3ZWIxZDMtNzdjZC00YmQwLWExYjctYzFhYmVlOWQ0MTRhLnBuZz9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDAxMTYlMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQwMTE2VDIwMzMyOFomWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPWJmOGE4NmVjNGQ3NTk5YmQ4ODc4OGY0NjU4MzlkYzRiNWM1NTc0NTY3ZDk4YzQxMDg3YjJiZGQ2ZTA2M2I3OGUmWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0JmFjdG9yX2lkPTAma2V5X2lkPTAmcmVwb19pZD0wIn0.wXCoLZw5vWz4KRFYTKHjHfxhPCyc2_eEGeBAUBFG3zM" style="width:700px;">

****

- Modelo: Predecir si una persona posee un mayor riesgo de desarrollar una enfermedad cardiovascular en función de sus características y hábitos de vida.
- Video: https://www.youtube.com/watch?v=mM-RTdBzRM0

[(project sources)](https://github.com/Jslp18/IA1-Project/blob/main/README.md)

---

## Dataset precio BITCOIN <a name="proy7"></a>

**Autores: Jhon Danilo Rincón Maldonado Miguel Fernando Pardo Maldonado Andres Joan Pardo Alquichire**

<img src="https://private-user-images.githubusercontent.com/77182773/290285015-7442756b-7758-495b-ac2a-449df88a3eea.png?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MDU0MzczNDYsIm5iZiI6MTcwNTQzNzA0NiwicGF0aCI6Ii83NzE4Mjc3My8yOTAyODUwMTUtNzQ0Mjc1NmItNzc1OC00OTViLWFjMmEtNDQ5ZGY4OGEzZWVhLnBuZz9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDAxMTYlMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQwMTE2VDIwMzA0NlomWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPWMwZDhhZGY4ZmM4NjgyYTY1YWRiYTg3MzlmZTk3YjljZjNiOTk4MjE1YmQ5YmQyODE0YmNmNjgzYmUyMTE1N2YmWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0JmFjdG9yX2lkPTAma2V5X2lkPTAmcmVwb19pZD0wIn0.pClhGdbsEppnpkkiCxOOS6kAkvPKgv64ugo95ktGxvk" style="width:700px;">

****

- Modelo: Predecir el ciclo de BITCOIN en una secuencia de datos.
- Video: https://www.youtube.com/watch?v=TCrbkVABmSU

[(project sources)](https://github.com/miguel0277/ProyectoIA)

---

## Red de calidad de aire - datos metereológicos <a name="proy8"></a>

**Autores: Alvaro Stiven Mejia**

<img src="https://raw.githubusercontent.com/Stevenmjo7/Proyecto-IA/main/2204006.jpg" style="width:700px;">

****

- Modelo: Predecir datos metereológicos
- Video: https://www.youtube.com/watch?v=KEiQdYGGlmc

[(project sources)](https://github.com/Stevenmjo7/Proyecto-IA)

---


## Clasificador de riesgo de cáncer de pulmón basado en los síntomas del paciente <a name="proy9"></a>

**Autores: Jorge Andrey García Vanegas - 2180115, Daniel Felipe Calderón Calderón - 2210052, Miguel Enrique Quintero Suarez - 2190932**

<img src="https://raw.githubusercontent.com/miguel2190932/Predicci-n_c-ncer_de_pulm-n/main/Banner.png" style="width:700px;">

**Objetivo: Determinar el riesgo de un cáncer de pulmón basado en los síntomas que este presenta.**

- Modelos: Support Vector Machine, Decision Tree Classifier, Random Forest Classifier.
- Video: https://www.youtube.com/watch?v=_LWYnfy13Mo

[(project sources)](https://github.com/miguel2190932/Predicci-n_c-ncer_de_pulm-n)
---


## Sistema de Predicción Temprana de Obesidad <a name="proy10"></a>

**Autores: Santiago Bolaños - 2201527, Dilan Rey - 2190397**

<img src="https://raw.githubusercontent.com/dilanerey06/ia_proyecto/main/img/banner_v2.jpg" style="width:700px;">

**Objetivo: Este proyecto tiene como enfoque principal tratar de clasificar (mediante distintos métodos de inteligencia artificial) personas expuestas a sufrir de obesidad teniendo en cuenta factores como lo son la alimentación, el ejercicio que realiza, la cantidad de agua que toma por día, etc.**

- Modelos: Gaussian Naive Bayes, Decision Tree, Random forest, Support Vector Machine (SVC), Deep Learning (DL), PCA.
- Video: https://www.youtube.com/watch?v=MMKUV2yXvmM

[(project sources)](https://github.com/dilanerey06/ia_proyecto)
---


## Royal Flush: Classification models for a Poker Hands Dataset <a name="proy11"></a>

**Autores: Isabella Callejas Mandon - 2202030, Geiner Duvan Guevara Vargas 2201840**

<img src="https://raw.githubusercontent.com/IC-03/Royal-Flush/main/Imagenes/Banner-GitHub-low_res.jpg" style="width:700px;">

**Objetivo: Clasificar manos de póquer según la combinación que presente las cartas.**

- Modelos: Gaussian Naive Bayes, Decision Tree,  Random Forest, SVM, Red Neuronal Densa (DNN), PCA, TSN-E, K-means, DBSCAN.
- Video: https://drive.google.com/file/d/1vWnw-nIVrzGQKXAuXR8vOpO1OIiLStyF/view

[(project sources)](https://github.com/IC-03/Royal-Flush)
---


## PD AI <a name="proy12"></a>

**Autores: Sebastián Mora - 2211554, Ángel Ortega: 2211583**

<img src="https://raw.githubusercontent.com/Anggelitozz/PD-AI/main/banner.jpg" style="width:700px;">

**Objetivo: clasificar entre pacientes de parkinson y control utilizando un dataset de imágenes de resonancia magnéticas cerebrales.**

- Modelos: SVC y red neuronal secuencial.
- Video: https://www.youtube.com/watch?v=DdTDnSSXfLk

[(project sources)](https://github.com/Anggelitozz/PD-AI)
---


## Clasificación de posiciones finales de pilotos en la Fórmula 1 <a name="proy13"></a>

**Autores: Anderson Acuña - 2191965, Deciré Jaimes - 2211835, Diego Sepúlveda - 2210063**

<img src="https://raw.githubusercontent.com/ThesplumCoder/formula1-ia-model/main/AI_POSTER.png" style="width:700px;">

**Objetivo: Utilizar los distintos métodos de clasificación vistos en clase para analizar datos históricos e intentar predecir la posición final del piloto.**

- Modelos: DecisionTree, RandomForest Classifier, Support Vector Classifier, PCA, DNN(Deep Neural Networks).
- Video: https://www.youtube.com/watch?v=c2aiWkuaYeA

[(project sources)](https://github.com/ThesplumCoder/formula1-ia-model)
---


## El ojo de bucar <a name="proy14"></a>

**Autores: Carlos Aceros**

<img src="https://raw.githubusercontent.com/carlosaceros/elojodebucar/main/El-ojo-de-bucar-banner.jpg" style="width:700px;">

**Objetivo: Implementar una herramienta web basada en inteligencia artificial que permita clasificar las zonas más peligrosas de la ciudad para poder implementar políticas públicas basadas en datos y lograr que la tranquilidad vuelva a la capital de Santander.**

- Modelos: Random forest, svm con kernels linear poly y rbf, decision tree, random forest, svc, red neuronal MLPC.
- Video: https://www.youtube.com/watch?v=VRd1Bpcu1DQ

[(project sources)](https://github.com/carlosaceros/elojodebucar)
---


## Predicción economía y precio de la BigMac <a name="proy15"></a>

**Autores: Brayan Arturo Navarro - 2201777, Camilo Andres Gamboa - 2202009**

<img src="https://raw.githubusercontent.com/Nbryan11/PROYECTO_IA_2023/main/banner.jpg" style="width:700px;">

**Objetivo: predecir el precio de la BigMac y analizar cambios en la economía de un país.**

- Modelos: Decision tree, Random forest, Maquina de soporte vectorial.
- Video: https://www.youtube.com/watch?v=UtoSMpRJEl8

[(project sources)](https://github.com/Nbryan11/PROYECTO_IA_2023)
---


## Estimación precios del Bitcoin <a name="proy16"></a>

**Autores: Adriana Gutierrez - 2205085, Andres Cardenas - 2204120, Ronald Arias - 2191927**

<img src="https://raw.githubusercontent.com/AdrianaGuti/Proyecto-IA/main/Banner.jpeg" style="width:700px;">

**Objetivo: Desarrollar un modelo predictivo que pueda analizar datos históricos y actuales para ofrecer pronósticos precisos sobre los movimientos de los precios de Bitcoin.**

- Modelos: PCA, DecisionTreeClassifier(Tunning y curva de aprendizaje), RandomForestClassifier(GridSearchCV y cross-validation),Support vector machine (Kernel rbf , kernel poly y kernel Sigmoid) y Red Neuronal.
- Video: https://www.youtube.com/watch?v=PAeEbt31Tkk

[(project sources)](https://github.com/AdrianaGuti/Proyecto-IA)
---


## SP-MUSIC_AI <a name="proy17"></a>

**Autores: Diego Fernando Marín Sisa - 2204005, Jhefry Ferney Martínez Celis - 2205002, Yosert Alejandro Higuera Lizarazo - 2205003**

<img src="https://raw.githubusercontent.com/Yagamzook302/SP-MUSIC_AI/main/Portada%20Proyecto%20SP-MUSIC_AI.png" style="width:700px;">

**Objetivo: Crear un sistema de recomendación del top 10 de las canciones más famosas hasta 2023 en Spotify y otras plataformas, con características adicionales más detalladas que otros conjuntos de datos similares basándonos en la popularidad de reproducción o streams.**

- Modelos: PCA, Regresión, Clasificación, kmeans, accuracy, MSE, MAE, RMSE, GaussianNB, DeccisionTreeRegressor, SVC, DT.
- Video: https://www.youtube.com/watch?v=8Ns-FQ4U4rc

[(project sources)](https://github.com/Yagamzook302/SP-MUSIC_AI)
---


## Diseño de lentes de contacto <a name="proy18"></a>

**Autores: Daniel Alejandro Sánchez Rodríguez - 2205620, Daniel Camilo Barrera Perez - 2205562**

<img src="https://raw.githubusercontent.com/daroap19/IA_Proyecto/main/Banner_IA.jpg" style="width:700px;">

**Objetivo: Observar que lente de contacto se adecua mejor según la enfermedad que tenga.**

- Modelos: Regresion, DNN, PCA, KMeans, T-sne, SuperVectorClassifier, DecisionTreeClassifier, RandomForestClassifier.
- Video: https://www.youtube.com/watch?v=VDyUA1muj7s

[(project sources)](https://github.com/daroap19/IA_Proyecto)
---


## Clasificador de aplicaciones Play Store <a name="proy19"></a>

**Autores: Cristian Stivens Villarreal Parra - 2204132, Joan Sebastian Patiño Jaimes - 2202052, Jorge Eduardo Suarez Cortes - 2205561**

<img src="https://raw.githubusercontent.com/Jspj1011/Play_Store_clasi/main/banner.jpg" style="width:700px;">

**Objetivo: El objetivo del proyecto es diseñar un modelo de clasificación que nos permita hallar la aplicación más popular en el conjunto de datos según diferentes variables como, su categoría, género, tipo, instalaciones, etc. Una vez entrenado, el clasificador podrá identificar la mejor aplicación en función de los criterios seleccionados.**

- Modelos: Random Forest Classifier, Decision Tree Classifier, Red Neuronal implementando adam, Red Neuronal implementando SGD.
- Video: https://www.youtube.com/watch?v=Qd5IGqSdaXA

[(project sources)](https://github.com/Jspj1011/Play_Store_clasi)
---


## Olimpics Winner Predictor <a name="proy20"></a>

**Autores: Luis Andres Gonzalez Corzo - 2201493, Santiago Gelvez Gonzalez - 2202046, Daniel Feipe Ballesteros Vesga - 2182786**

<img src="https://raw.githubusercontent.com/luis3132/AI-Proyect/main/banner.png" style="width:700px;">

**Objetivo: El proyecto se centra en la aplicación de algoritmos para predecir qué países tienen mayores probabilidades de ganar medallas en los Juegos Olímpicos. Utilizando datos históricos, variables demográficas, temporadas y deportivas, se implemento varios algoritmos de aprendizaje automático para crear un modelo predictivo. El dataset cuenta con un historial tomado desde 1896 hasta 2016.**

- Modelos: Decision Tree Classifier, Deep Learning.
- Video: https://www.youtube.com/watch?v=_cRrAUJXePw

[(project sources)](https://github.com/luis3132/AI-Proyect)
---


## Medicamentos Vigentes <a name="proy21"></a>

**Autores: Juan José Rincón Méndez - 2202018, Dylan Francisco Jimenez Sandoval - 2202049, Sonia Marcela Granados Moreno - 2204250**

<img src="https://camo.githubusercontent.com/752561fc996acba1b08f107af6949b265732ac8c2f210dc2e52ceacd3f936cb3/68747470733a2f2f692e706f7374696d672e63632f7843596a436478512f57686174732d4170702d496d6167652d323032332d31322d30362d61742d31312d34332d33332d504d2e6a7067" style="width:700px;">

**Objetivo: Se busca conocer cual laboratorio tiene un medicamento con mayor tiempo de duración del producto, con lo cual poder conocer que producto dura mas con el laboratorio correspondiente.**

- Modelos: Regresion lineal, RandomForest, DecissionTree, PCA, TensorFlow, TimeStamp.
- Video: https://www.youtube.com/watch?v=7KIMqUSoWRE

[(project sources)](https://github.com/DylanFJS/IA-PROYECTO-FINAL)
---


## Detección de anomalías en trenes <a name="proy22"></a>

**Autores: Jose Miguel Pardo 2202004 - Andrey Felipe Leguizamo 2201998 - Juan Sebastian Suarez 2201993**

<img src="https://raw.githubusercontent.com/mxgue13/Deteccion-de-anomalias-en-trenes/main/BannerProyecto.png" style="width:700px;">

**Objetivo: Crear un modelo preciso y confiable que emplee información ferroviaria para identificar irregularidades en la operación de trenes.**

- Modelos: GaussianNaiveBayes, DecisionTreeClassifier, RandomForestClassifier, SupportVectorMachine, Aprendizaje no supervisado (PCA), Deep Learning (Redes neuronales).
- Video: https://www.youtube.com/watch?v=sBlEP5bvc5A

[(project sources)](https://github.com/mxgue13/Deteccion-de-anomalias-en-trenes)
---


## Clasificación de cuentas falsas de Instagram con IA <a name="proy23"></a>

**Autores: Andrea Parra - 2210062, Silvia Cárdenas - 2210102, Paula Uzcátegui - 2211475**

<img src="https://raw.githubusercontent.com/PaulaUzca/Clasificacion-Perfiles-IA1/main/banner.jpeg" style="width:700px;">

**Objetivo: Comparar diversos métodos de clasificación con IA(tanto supervisados como no supervisados) y evaluar su desempeño a la hora de identificar perfiles falsos de instagram.**

- Modelos: Gaussian Naive Bayes, Decision Tree, Random Forest, SVM, Deep Learning MLP, KMeans, DBScan.
- Video: https://www.youtube.com/watch?v=x0od9PkOFG8

[(project sources)](https://github.com/PaulaUzca/Clasificacion-Perfiles-IA1)
---


## Pronóstico de aceptación de depósito a plazo <a name="proy24"></a>

**Autores: Freddy Santiago Galán Figueroa - 2201520**

<img src="https://github.com/FreddyG-404/ProyectoIA/blob/main/BannerProyecto.png?raw=true" style="width:700px;">

**Objetivo: El objetivo del proyecto es utilizar inteligencia artificial para predecir la suscripción de clientes a depósitos a plazo en un banco portugués, empleando una red neuronal y técnicas de validación cruzada para mejorar la precisión del modelo.**

- Modelos: Decission Tree, SVC, Red neuronal, PCA
- Video: https://www.youtube.com/watch?v=WRNL3jwVvU0

[(project sources)](https://github.com/FreddyG-404/ProyectoIA)
---


## Análisis de Riesgo Crediticio <a name="proy25"></a>

**Autores: Donovan David Torres Vahos - 2204125, Karen Tatiana Pimiento Martinez - 2204129**

<img src="https://raw.githubusercontent.com/DonovanDavid/Analisis-Riegos-Crediticio/main/banner%20riesgo%20crediticio.png" style="width:700px;">

**Objetivo: El objetivo del proyecto es aplicar modelos de clasificación para evaluar el riesgo crediticio de las personas, considerando diversas variables como la intención del préstamo, edad, historial crediticio, entre otras. Además, este proyecto permitirá profundizar en conceptos y técnicas aprendidas en clase.**

- Modelos: Decision Tree Regressor, Regresión de Vector de Soporte, Red neuronal de clasificación y PCA.
- Video: https://www.youtube.com/watch?v=sDVNi2U6-hc

[(project sources)](https://github.com/DonovanDavid/Analisis-Riegos-Crediticio)
---
