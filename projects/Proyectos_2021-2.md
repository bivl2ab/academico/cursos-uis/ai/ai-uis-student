---
# Proyectos 2021-2. Inteligencia Artificial. 

## Prof: Fabio Martínez, Ph.D
## Prof: Gustavo Garzón
---

# Lista de Proyectos
1. [Determinación del estadío de la enfermedad de alzheimer a través de imagenología MRI](#proy1)
2. [Pollair](#proy2)
3. [SAGRILAFT](#proy3)
4. [StudifyIA](#proy4)
5. [Detección de somnolencia en conductores](#proy5)
6. [Monitor y predictor de ovulación por medio del método de temperatura basal](#proy6)
7. [Factores importantes para detectar cancer de mama](#proy7)
8. [FAKE REVIEW Classifier-inator](#proy8)
9. [Predicting depression, anxiety and stress](#proy9)
10. [Sistema de Detección del Uso de Tapabocas](#proy10)
11. [Sistema de prevención de pacientes con ataques cardíacos](#proy11)
12. [Detección y reconocimiento de matrículas con Deep Learning](#proy12)
13. [Predicción de posibles robos en el baseball](#proy13)
14. [Sistema de predición del indice de contaminación en el aire en Colombia](#proy14)
15. [Chinese Numbers Recognition: A DNN Approach](#proy15)
16. [Water Quality](#proy16)
17. [Detección de posibles casos de COVID-19 a base del sonido de la tos](#proy17)
18. [Soccer-Predictor](#proy18)
19. [Detección de enfermedades pulmonares por medio de radiografias de tórax](#proy19)
20. [TweCla](#proy20)
21. [Detección de Uso de Tapabocas](#proy21)
22. [Potabilidad Del Agua](#proy22)

---

## Determinación del estadío de la enfermedad de alzheimer a través de imagenología MRI <a name="proy1"></a>

**Autores: Daniel Eduardo Ortiz Celis, Camilo Eduardo González Guerrero**

<img src="https://user-images.githubusercontent.com/73614233/156952723-32c7b6b8-896b-4ffc-8c9a-28442b856aa1.png" style="width:700px;">

**Objetivo: Herramienta para la detección de estadío de la enfermedad de Alzheimer a través de imagenología MRI.**

- Dataset: Alzheimer's Dataset [(link)](https://www.kaggle.com/tourist55/alzheimers-dataset-4-class-of-images)
- Modelo: Gaussian Naive Bayes, Decision Tree classifier, Random Forest classifier, Support vector machine (SVM) con 3 kernels (RBF, poly y linear), Redes neuronales y Redes convolucionales.

[(code)](https://github.com/ky02u/Proyecto_IA/blob/main/Proyecto_IA_MRI.ipynb) [(video)](https://www.youtube.com/watch?v=SguUOUBv6LE) [(+info)](https://github.com/ky02u/Proyecto_IA)

---

## Pollair <a name="proy2"></a>

**Autores: Erika Yamile Lache Blanco**

<img src="https://github.com/erikalache97/Proyecto--IA/raw/main/BannerIA.png" style="width:700px;">

**Objetivo: Sistema de predicción de agentes contaminantes en el aire en Colombia-Medellín.**

- Dataset: OpenAQ - Colombia [(link)](https://openaq.org/#/countries/CO)
- Modelo: DecisionTreeRegressor, RandomForestRegressor, SVR y red neuronal.

[(code)](https://github.com/erikalache97/Proyecto--IA/blob/main/Proyecto_IA_Pm25.ipynb) [(video)](https://www.youtube.com/watch?v=EL594MceVII) [(+info)](https://github.com/erikalache97/Proyecto--IA)

---

## SAGRILAFT <a name="proy3"></a>

**Autores: Johan Suarez, Efrain Blanco, Luis Linares**

<img src="https://user-images.githubusercontent.com/70672226/157332196-7940fa64-81a6-4a16-bc92-286176057d6d.jpg" style="width:700px;">

**Objetivo: Detectar y tomar medidas frente a actos delictivos de una empresa.**

- Dataset: (privado)
- Modelo: GaussianNB, Random Forest Classifier, Decision Tree Classifier, Support Vector Machine.

[(code)](https://gitlab.com/JOSESUCH/sistema-de-riesgo/-/blob/main/Sistema_de_detecci%C3%B3n_de_riesgo__1_.ipynb) [(video)](https://www.youtube.com/watch?v=jXsrGVZmfLc) [(+info)](https://github.com/JOSESUCH/SAGRILAFT)

---

## StudifyIA <a name="proy4"></a>

**Autores: Carlos Sanjuan, Cristian Fuentes, Rances Rodriguez**

<img src="https://github.com/csanjuan99/ia-students-predictions-project/raw/main/banner.png" style="width:700px;">

**Objetivo: Predecir notas finales de un curso de un estudiando con respecto a ciertas caracteristicas demograficas, sociales y escuelas.**

- Dataset: Predicting Student Performance [(link)](https://www.kaggle.com/yamqwe/student-performance)
- Modelo: DecisionTreeRegressor, RandomForestRegressor, SupportVectorRegressor.

[(code)](https://github.com/csanjuan99/ia-students-predictions-project/blob/main/ia-project.ipynb) [(video)](https://www.youtube.com/watch?v=OI81V1BZkOU) [(+info)](https://github.com/csanjuan99/ia-students-predictions-project)

---

## Detección de somnolencia en conductores <a name="proy5"></a>

**Autores: Najhery Soler Maldonado, Iveth Medina Reatiga, Jhon Uribe González**

<img src="https://raw.githubusercontent.com/iveth1/Proyecto-IA/main/somnolencia.jpg" style="width:700px;">

**Objetivo: Crear un sistema que permita detectar si un conductor presenta signos de somnolencia.**

- Dataset: yawn_eye_dataset_new [(link)](https://www.kaggle.com/serenaraju/yawn-eye-dataset-new)
- Modelo: Gaussian NB, RandomForestClassifier, DecisionTreeClassifier, SupportVectorClassifier, Red neuronal convolucional.

[(code)](https://github.com/iveth1/Proyecto-IA/blob/main/proyecto.ipynb) [(video)](https://www.youtube.com/watch?v=wDzE3vckpWM) [(+info)](https://github.com/iveth1/Proyecto-IA)

---

## Monitor y predictor de ovulación por medio del método de temperatura basal <a name="proy6"></a>

**Autores: Valentina Galvis, Laura Hernandez, Neyder Mosquera**

<img src="https://github.com/valegb13/Proyecto-IA/raw/main/Proyecto.gif" style="width:700px;">

**Objetivo: Predecir correctamente la ovulación por medio de la toma de la temperatura basal teniendo en cuenta bases de datos y patrones generales en este ciclo.**

- Dataset: [(link)](https://github.com/valegb13/Proyecto-IA/blob/main/Dataset.csv)
- Modelo: Regresión lineal, DecisionTree Regressor, RandomForest Regressor.

[(code)](https://github.com/valegb13/Proyecto-IA/blob/main/PROYECTO_IA.ipynb) [(video)](https://www.youtube.com/watch?v=Zf5bs4uyEP8) [(+info)](https://github.com/valegb13/Proyecto-IA)

---

## Factores importantes para detectar cancer de mama <a name="proy7"></a>

**Autores: Jessica Marcela Ariza Florez**

<img src="https://user-images.githubusercontent.com/98856036/156943801-857f25f4-173b-413b-b0a0-7471bfed8415.png" style="width:700px;">

**Objetivo: Determinar cuales son los factores importantes a la hora de detectar el cancer de mama.**

- Dataset: [(link)](https://github.com/jessica9988/proyecto-ia/blob/main/proyecto-IA/data/breast-cancer.csv)
- Modelo: Random Forest Classifier y decision Tree Classifier

[(code)](https://github.com/jessica9988/proyecto-ia/blob/main/proyecto-IA/cancermama.ipynb) [(video)](https://www.youtube.com/watch?v=Yk7Q7zgxhvU) [(+info)](https://github.com/jessica9988/proyecto-ia)

---

## FAKE REVIEW Classifier-inator <a name="proy8"></a>

**Autores: Daniel Cervantes, Edward Parada, Gianfranco Estevez**

<img src="https://i.imgur.com/xRp1fVS.png" style="width:700px;">

**Objetivo: Desarrollar una inteligencia artificial capaz de diferenciar entre reseñas de productos falsas y verdaderas.**

- Dataset: fake reviews dataset [(link)](https://osf.io/3vds7/)
- Modelo: Decision Tree Classifier, Support Vector Classifier, K-Means, Cross-Validation

[(code)](https://github.com/Gacrucis/WhoReviewsTheReviews/blob/main/WhoReviewsTheReviews.ipynb) [(video)](https://www.youtube.com/watch?v=vqwTscdIfr8) [(+info)](https://github.com/Gacrucis/WhoReviewsTheReviews)

---

## Predicting depression, anxiety and stress <a name="proy9"></a>

**Autores: Juan Esteban Duarte Rueda, Juan Jose Prenss Castro**

<img src="https://raw.githubusercontent.com/JuanDev12/PredictingDepression/main/Banner%20(1).png" style="width:700px;">

**Objetivo: predecir casos de ansiedad, depresión y estres a partir de modelos entrenados a partir de la encuesta DASS (Depression, Anxiety and Stress Scale).**

- Dataset: Predicting Depression, Anxiety and Stress [(link)](https://www.kaggle.com/yamqwe/depression-anxiety-stress-scales)
- Modelo: RandomForestClassifier, DecisionTreeClassifier, SVC, NeuralNetwork

[(code)](https://github.com/JuanDev12/PredictingDepression/blob/main/Proyecto-IA.ipynb) [(video)](https://www.youtube.com/watch?v=c4BcvsHTqwk) [(+info)](https://github.com/JuanDev12/PredictingDepression)

---

## Sistema de Detección del Uso de Tapabocas <a name="proy10"></a>

**Autores: Brandon Arley Archila Anaya, Brayan Andrés Quintero Pinto, Juan Pablo Ramírez Vela**

<img src="https://user-images.githubusercontent.com/66091214/157054151-d9c8f00e-b2c4-4218-8b4d-7f7c057a0ff8.png" style="width:700px;">

**Objetivo: Objetivo: Detectar el buen uso de tapabocas.**

- Dataset: [(link)](https://github.com/branbot1046/SDUTP/tree/main/Fotos)
- Modelo: MTCNN, Convolutional Neural Networks

[(code)](https://github.com/branbot1046/SDUTP/blob/main/Proyecto_Final_IA_SDUTP.ipynb) [(video)](https://www.youtube.com/watch?v=vcoDRfQLIkE) [(+info)](https://github.com/branbot1046/SDUTP)

---

## Sistema de prevención de pacientes con ataques cardíacos <a name="proy11"></a>

**Autores: Jorge Andrés Cárdenas Parada, Daniel Santiago Torres Salcedo, Isnardo Corredor Ariza**

<img src="https://raw.githubusercontent.com/JuniorXD0301/proyecto-IA/main/Banner.jpg" style="width:700px;">

**Objetivo: Analizar los factores que determinen si un paciente podría sufrir de un ataque cardíaco. Diagnosticar a los pacientes la posibilidad de un ataque cardíaco en una fase más temprana.**

- Dataset: Heart Disease prediction [(link)](https://www.kaggle.com/lakhankumawat/heart-disease-prediction/data)
- Modelo: Decission Tree Classifier, Random Forest Classifier

[(code)](https://github.com/JuniorXD0301/proyecto-IA/blob/main/Notebook_proyecto.ipynb) [(video)](https://www.youtube.com/watch?v=CUE0fPpSgb4) [(+info)](https://github.com/JuniorXD0301/proyecto-IA)

---

## Detección y reconocimiento de matrículas con Deep Learning <a name="proy12"></a>

**Autores: Maria Paula Rodriguez, Helman Andres Merchan Quevedo, Nicolas Stiven Jaimes**

<img src="https://gitlab.com/Stiven77.nj/proyecto-ia-matriculas-de-vehiculos/-/raw/main/Banner_-_PROYECTO_IA.png" style="width:700px;">

**Objetivo: Clasificar imágenes de vehículos según si presentan matrículas o no, y extraer de una imagen la matrícula de un carro (en formato texto).**

- Dataset: [(link)](https://drive.google.com/drive/folders/1cm7BmdCoGr1XTPwLZYMd7HTa6T-aDTLR)
- Modelo: Gaussian NB, RandomForestClassifier, DecisionTreeClassifier, SupportVectorClassifier, Red Neuronal Profunda, Red Neuronal Convolucional

[(code)](https://gitlab.com/Stiven77.nj/proyecto-ia-matriculas-de-vehiculos/-/blob/main/Proyecto.ipynb) [(video)](https://www.youtube.com/watch?v=H5gCQNH9YSY) [(+info)](https://gitlab.com/Stiven77.nj/proyecto-ia-matriculas-de-vehiculos)

---

## Predicción de posibles robos en el baseball <a name="proy13"></a>

**Autores: Juan Sebastian Vanegas Rico, Juan José Bayona Sepúlveda**

<img src="https://raw.githubusercontent.com/JuanJoseBayonaSepulveda/PrediccionRoboDeBases/main/Banner.png" style="width:700px;">

**Objetivo: Lograr predecir con un alto grado de certeza en que situación realmente un corredor va a robar una base.**

- Dataset: [(link)](https://raw.githubusercontent.com/JuanJoseBayonaSepulveda/PrediccionRoboDeBases/main/data_ia_proyecto_ABC.csv)
- Modelo: Gaussian NB, RandomForestClassifier, DecisionTreeClassifier, SuportVectorClassifier, DNN.

[(code)](https://github.com/JuanJoseBayonaSepulveda/PrediccionRoboDeBases/blob/main/Predicci%C3%B3nDeRobosEnBaseball.ipynb) [(video)](https://www.youtube.com/watch?v=iFulDxMPwqo) [(+info)](https://github.com/JuanJoseBayonaSepulveda/PrediccionRoboDeBases)

---

## Sistema de predición del indice de contaminación en el aire en Colombia <a name="proy14"></a>

**Autores: Ivan Mendoza Oñate, Deyci Toloza Ortega**

<img src="https://github.com/ivan2171977/project-IA/raw/main/Proyecto%20polucion%20banner.png" style="width:700px;">

**Objetivo: creacion de un sistema regresor que pueda predecir el indice de contaminacion en el aire en colombia.**

- Dataset: OpenAQ - Colombia [(link)](https://openaq.org/#/countries/CO)
- Modelo: Random Forest regresor, decision tree regresor, redes neuronales, PCA, SVM

[(code)](https://github.com/ivan2171977/project-IA/blob/main/project_v1%20(1).ipynb) [(video)](https://www.youtube.com/watch?v=Srbm6LQaIEQ) [(+info)](https://github.com/ivan2171977/project-IA)

---

## Chinese Numbers Recognition: A DNN Approach <a name="proy15"></a>

**Autores: Julián Esteban García Duarte, Paula Catalina Hernandez, Paula Andrea Castro Mendoza**

<img src="https://raw.githubusercontent.com/JulianGarciaDuarte/chinese_mnist_dnn/main/banner.png" style="width:700px;">

**Objetivo: Crear un clasificador de números en caracteres chinos escritos a mano.**

- Dataset: Chinese MNIST [(link)](https://www.kaggle.com/gpreda/chinese-mnist)
- Modelo: DNN, Decition Tree, Random Forest, Support Vector Machine.

[(code)](https://github.com/JulianGarciaDuarte/chinese_mnist_dnn/blob/main/Proyecto_IA_Chinese_mnist_recognition.ipynb) [(video)](https://www.youtube.com/watch?v=0vzQpLJkHpU) [(+info)](https://github.com/JulianGarciaDuarte/chinese_mnist_dnn)

---

## Water Quality <a name="proy16"></a>

**Autores: Mauricio Alejandro Romero Jaimes, Daniel Baez Acevedo**

<img src="https://raw.githubusercontent.com/mauro3001/proyecto-ia/main/water-quality.png" style="width:700px;">

**Objetivo: Evaluar sustancias físico-químicas presentes en el agua, según diversos estándares.**

- Dataset: Water Quality [(link)](https://www.kaggle.com/mssmartypants/water-quality)
- Modelo: Gaussian NB, RandomForestClassifier, DecisionTreeClassifier, SuportVectorMachine (SVM)

[(code)](https://github.com/mauro3001/proyecto-ia/blob/main/proyecto_ia.ipynb) [(video)](https://www.youtube.com/watch?v=z_xDHXIuWpE) [(+info)](https://github.com/mauro3001/proyecto-ia)

---

## Detección de posibles casos de COVID-19 a base del sonido de la tos <a name="proy17"></a>

**Autores: Daniel Alejandro León Ortiz, David Mauricio Gallo Franco**

<img src="https://raw.githubusercontent.com/DALO-eng/Proyecto-IA-Covid-by-Tos/master/media/Banner.png" style="width:700px;">

**Objetivo: Estudiar diferentes aspectos que contiene la tos humana a partir de audios con el objetivo de predecir un posible caso de Covid-19, mediante un problema de clasificación binaria.**

- Dataset: SpartaCov [(link)](https://www.kaggle.com/neptuneapps/spartacov-detect-covid-from-cough)
- Modelo: GaussianNB, DecisionTree, RandomForest, Deep Learning

[(code)](https://github.com/DALO-eng/Proyecto-IA-Covid-by-Tos/blob/master/notebooks/IAProject-Cough.ipynb) [(video)](https://www.youtube.com/watch?v=MfIFwNXuHVM) [(+info)](https://github.com/DALO-eng/Proyecto-IA-Covid-by-Tos)

---

## Soccer-Predictor <a name="proy18"></a>

**Autores: Alejandro Romero Serrano, Dubian Enrique Palacios Rivera, Jorge Sandoval**

<img src="https://raw.githubusercontent.com/georsan/Soccer-Predictor/main/Soccer-predictor.jpeg" style="width:700px;">

**Objetivo: Predecir el resultados de cada partido que se jugara en la premier league.**

- Dataset: Football Database [(link)](https://www.kaggle.com/technika148/football-database)
- Modelo: DecisionTreeRegressor, SVR, Random Forest Regressor, DNN

[(code)](https://github.com/georsan/Soccer-Predictor/blob/main/Proyecto.ipynb) [(video)](https://www.youtube.com/watch?v=yKT4MKwaUEE) [(+info)](https://github.com/georsan/Soccer-Predictor)

---

## Detección de enfermedades pulmonares por medio de radiografias de tórax <a name="proy19"></a>

**Autores: Jose Fabian Jimenez Ovalle, Javier Fernando Carvajal Sanabria, Juan Sebastian Mora Rueda**

<img src="https://raw.githubusercontent.com/fabian017/INTELIGENCIA-ARTIFICIAL/main/img/Banner.png" style="width:700px;">

**Objetivo: Con el desarrollo de este proyecto se busca crear una ayuda para los profesionales de la salud en la deteccion de enfermedades pulmonares en sus pacientes.**

- Dataset: COVID-19 Radiography Dataset [(link)](https://www.kaggle.com/preetviradiya/covid19-radiography-dataset)
- Modelo: Gaussian Naive Bayes, Decission Tree Classifier, Random Forest Classifier, Support Vector Machine, Redes Neuronales, Redes Convolucionales

[(code)](https://colab.research.google.com/drive/1fh_KvQSPLX92jmoCz-4ZqvpGdqhlnN1J?usp=sharing) [(video)](https://www.youtube.com/watch?v=UXmtEJ3NN4U) [(+info)](https://github.com/fabian017/Inteligencia_Artificial-1)

---

## TweCla <a name="proy20"></a>

**Autores: José Silva, Yuri Melissa, Jhan Rojas**

<img src="https://github.com/VashLT/TweCla/raw/master/banner.png" style="width:700px;">

**Objetivo: Classify tweets by basic emotions.**

- Dataset: Emotion Dataset for Emotion Recognition Tasks [(link)](https://www.kaggle.com/parulpandey/emotion-dataset?select=training.csv)
- Modelo: Deep Neural Network, Support Vector Machine, Random Forest, Logistic Regression (multibinomial for multiclasses)

[(code)](https://github.com/VashLT/TweCla/blob/master/TweCla.ipynb) [(video)](https://www.youtube.com/watch?v=1TsA4zPwmjg) [(+info)](https://github.com/VashLT/TweCla)

---

## Detección de Uso de Tapabocas <a name="proy21"></a>

**Autores: Anderson Andres Gonzalez Cortes, Elsyn Andrea Vargas Ramirez**

<img src="https://user-images.githubusercontent.com/89708735/157370406-1fef37c3-840d-48d5-a85d-b02be0d20487.jpg" style="width:700px;">

**Objetivo: Reconocimiento del uso del tapabocas gracias al entrenamiento de inteligencia artificial.**

- Dataset: Face Mask Detection [(link)](https://www.kaggle.com/vijaykumar1799/face-mask-detection)
- Modelo: DNN, DecisionTree, RandomForest, CNN

[(code)](https://github.com/ElsynV/Deteccion-mascarillas/blob/main/PROYECTO.ipynb) [(video)]() [(+info)](https://github.com/ElsynV/Deteccion-mascarillas)

---

## Potabilidad Del Agua <a name="proy22"></a>

**Autores: Yesid Romario Gualdrón Hurtado, Amin Esteban Barbosa Vargas, Ruben Dario Rodríguez Moreno**

<img src="https://raw.githubusercontent.com/RubenR-M/PotabilidadDelAgua/main/banner.png" style="width:700px;">

**Objetivo: Identificar la calidad del agua de los departamentos y municipios en Colombia.**

- Dataset: Indice de Riesgo por Calidad del Agua para Consumo Humano (IRCA) Municipal 2010-2019 en Colombia [(link)](https://data.humdata.org/dataset/indice-de-riesgo-por-calidad-del-agua-para-consumo-humano-irca-municipal-2010-2019-en-colombia)
- Modelo: RandomForestRegressor

[(code)](https://github.com/RubenR-M/PotabilidadDelAgua/blob/main/CalidadAgua.ipynb) [(video)](https://www.youtube.com/watch?v=uBh9U5CAZFg) [(+info)](https://github.com/RubenR-M/PotabilidadDelAgua)

---
