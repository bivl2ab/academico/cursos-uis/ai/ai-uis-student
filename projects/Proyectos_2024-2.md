---
# Proyectos 2024-2. Inteligencia Artificial.

## Prof: Fabio Martínez, Ph.D
## Prof: Gustavo Garzón
---

# Lista de Proyectos
1. [Diabetes-IA](#proy1)
2. [MachineLearningwithHouses](#proy2)
3. [Canciones](#proy3)
4. [Residuos](#proy4)
5. [Salud Mental](#proy5)
6. [Posiciones en NBA](#proy6)
7. [Phishhing](#proy7)
8. [Rostros](#proy8)


---

## Diabetes-IA <a name="proy1"></a>

**Autores: Michael Ronaldo Rueda Barragán, Nataly Cristina Ortíz Salinas.**

<img src="https://camo.githubusercontent.com/ddcc17165a1c9f005780965c1a03e60820f9f9f1ba883e51d89ad2b742140381/68747470733a2f2f692e6962622e636f2f5766725a3152722f556e7469746c65642e6a7067" style="width:700px;">

****

- Utilizar algoritmos entrenados con inteligencia artificial para predecir la diabetes en individuos, basándonos en datos como edad, sexo, IMC, hipertensión, cardiopatías, historial de tabaquismo, nivel de HbA1c y glucosa en la sangre.
- Video: https://youtu.be/l61ppLkPrUE

[(project sources)](https://github.com/MichaelRueda/Proyecto-Diabetes-IA)

---

## MachineLearningwithHouses <a name="proy2"></a>

**Autores: Yeison Adrian Caceres Torres, Ricardo Svensson Jaimes Estupiñan, Darien Andres Castañeda Agudelos**

<img src="https://github.com/RicardoJaimes04/MachineLearningwithHouses/blob/main/banner.png?raw=true" style="width:700px;">

****

- Predecir el precio de una vivienda en Colombia, dadas unas características. Ayudar a seleccionar vivienda a el usuario.
- Video: https://youtu.be/H6GAvFi5fRU

[(project sources)](https://github.com/RicardoJaimes04/MachineLearningwithHouses?tab=readme-ov-file)

---

## Prediccion de genero de canciones <a name="proy3"></a>

**Autores: Juan Felipe Rojas de la Hoz, Diego Alejandro Arevalo Quintero**

<img src="https://private-user-images.githubusercontent.com/58739212/393959986-bfaa77a0-1e38-402b-8edd-2d19e5d8b2b4.png?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MzM5NTkxMTcsIm5iZiI6MTczMzk1ODgxNywicGF0aCI6Ii81ODczOTIxMi8zOTM5NTk5ODYtYmZhYTc3YTAtMWUzOC00MDJiLThlZGQtMmQxOWU1ZDhiMmI0LnBuZz9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDEyMTElMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQxMjExVDIzMTMzN1omWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPThjOGZjMjllYmM0NzRlMzYxMTdhNTcxM2E5NWU3NGQwZDMzMDk3MGNmYzFjNDBlOWFmMWRhZDZiN2U0NGM3MTImWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0In0.KzubpVNcH4d0jM01aJQgkJw-0IEi_c08ZiXPEXD9y90" style="width:700px;">

****

- Identificar el genero de las canciones usando su letra


- Video: https://youtu.be/W_0Ur_CK7lI

[(project sources)](https://github.com/juanFeRoz/prediccion-genero-canciones-con-letra?tab=readme-ov-file)

---


## Clasificación de residuos <a name="proy4"></a>

**Autores: Andrés Felipe Jaimes Ojeda, Anderson Jahir Lemus Ramírez , Hammer Ronaldo Muñoz Hernandez**

<img src="https://github.com/NDruz/Clasificacion-de-resudios/blob/main/Banner.jpg?raw=true" style="width:700px;">

****

- Sistema de detección y clasificación de residuos utilizando inteligencia artificial




- Video: https://www.youtube.com/watch?v=6HLw5H6IT74

[(project sources)](https://github.com/NDruz/Clasificacion-de-resudios?tab=readme-ov-file)

---

## Proyecto-final-IA/Asistente Personal de Salud Mental <a name="proy5"></a>

**Autores: Jhoan Sebastian Garcia Reyes - 2202045, Daniel Tarazona Sanchez - 2210097, Juan Tabares - 2191712**

<img src="https://github.com/NDruz/Clasificacion-de-resudios/blob/main/Banner.jpg?raw=true" style="width:700px;">

****

-  identificar las preguntas y características más relevantes para predecir estos estados mediante técnicas de aprendizaje automático y reducción dimensional.




- Video: https://youtu.be/a1UOcUpTfVg

[(project sources)](https://github.com/DaniellTarazona26/proyecto-final-IA)

---

## Clasificador de posiciones NBA <a name="proy6"></a>

**Autores: Jose Daniel Figueroa Arenas, Kevin Daniel Castro Mendoza, Juan Camilo Velandia Amortegui**

<img src="https://private-user-images.githubusercontent.com/124414206/393659473-d69205b3-5ba2-4026-b2de-fb0e9b2da52d.png?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MzM5NTk0NzAsIm5iZiI6MTczMzk1OTE3MCwicGF0aCI6Ii8xMjQ0MTQyMDYvMzkzNjU5NDczLWQ2OTIwNWIzLTViYTItNDAyNi1iMmRlLWZiMGU5YjJkYTUyZC5wbmc_WC1BbXotQWxnb3JpdGhtPUFXUzQtSE1BQy1TSEEyNTYmWC1BbXotQ3JlZGVudGlhbD1BS0lBVkNPRFlMU0E1M1BRSzRaQSUyRjIwMjQxMjExJTJGdXMtZWFzdC0xJTJGczMlMkZhd3M0X3JlcXVlc3QmWC1BbXotRGF0ZT0yMDI0MTIxMVQyMzE5MzBaJlgtQW16LUV4cGlyZXM9MzAwJlgtQW16LVNpZ25hdHVyZT1iMjAzODFiYzIyMzk4Y2FkMmIyMDBjMzNiYTRlMGJkYTNlMTkyMjFkZTliYjBiODBiMGNjZWM1OGI5YWQ3YjJlJlgtQW16LVNpZ25lZEhlYWRlcnM9aG9zdCJ9.3g1sPBpov8pIpcpJ7AKM97Z-33lCIvg11A9gE8cIElY" style="width:700px;">

****

-  El objetivo de este proyecto es desarrollar un clasificador basado en inteligencia artificial capaz de identificar la posición en la que juega un jugador de la NBA a partir de sus estadísticas personales dentro de la cancha. Utilizando un conjunto de datos de rendimiento de los jugadores, que incluye métricas como puntos por partido, asistencias, rebotes, robos, bloqueos, entre otras, el modelo de clasificación será entrenado para predecir la posición en la que un jugador desempeña su rol, como base (PG), escolta (SG), alero (SF), ala-pívot (PF) o pívot (C).




- Video: https://www.youtube.com/watch?v=p2GKrBqFw00

[(project sources)](https://github.com/danfigueroa97/FinalProjectAI_NBAClassifier/tree/main?tab=readme-ov-file)

---

## Detección de Phishing utilizando Modelos de Aprendizaje Automáticos y Redes Neuronales <a name="proy7"></a>

**Autores: Julian Andres Jaramillo, Javier David Villanueva, Jefferson Holguin**

<img src="https://private-user-images.githubusercontent.com/70348839/394372638-37e5c608-6433-4c86-b964-d9f751d7b807.jpg?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MzQwMDU1ODksIm5iZiI6MTczNDAwNTI4OSwicGF0aCI6Ii83MDM0ODgzOS8zOTQzNzI2MzgtMzdlNWM2MDgtNjQzMy00Yzg2LWI5NjQtZDlmNzUxZDdiODA3LmpwZz9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDEyMTIlMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQxMjEyVDEyMDgwOVomWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPWE5MzJhZDJkYTE5MzRlY2FiMDdjMjM4N2Y4YzQxYjBjOWU2ZGEwOWI3YWY0ZDllNDg5MDUxYjZhMGEzMTExZjUmWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0In0.w1wwvYzMhSFrtyP9xNsOsdqdaNIdarmRLgspeEUSXQ4" style="width:700px;">

****

-  Clasificar URLs utilizando machine learning y modelos/metodos vistos en la materia Inteligencia Atrificial 1.




- Video:https://www.youtube.com/watch?v=jPnKU1WabUo

[(project sources)](https://github.com/J4DR3Z/IA_Phishhing?tab=readme-ov-file)

---

## Detección y Clasificación de Rostros para Aplicaciones de Seguridad <a name="proy8"></a>

**Autores: Diego Alejandro García Barajas - 2211848 , Jerson Steven Mantilla Ramírez - 2211849 , Ana Valeria Barreto Téllez - 2215630**

<img src="https://raw.githubusercontent.com/jerson1210/AI-proyecto/refs/heads/main/Deteccion%20(1).png" style="width:700px;">

****

-  Detectar y clasificar rostros para mejorar la seguridad en aplicaciones tecnológicas.




- Video:https://www.youtube.com/watch?v=4SxMffNWd0Y&feature=youtu.be

[(project sources)](https://github.com/jerson1210/AI-proyecto?tab=readme-ov-file)

