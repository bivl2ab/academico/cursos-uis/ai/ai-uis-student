---
# Proyectos 2021-1. Inteligencia Artificial. 

## Prof: Fabio Martínez, Ph.D
## Prof: Gustavo Garzón
---

# Lista de Proyectos
1. [Predictor de vilencia](#proy1)
2. [MODELO DE DETECCION DE ESTADO DE SEMAFOROS PARA AYUDAR A PERSONAS INVIDENTES](#proy2)
3. [EmotionsAI](#proy3)
4. [Clasificación Multiclase para Medidas Correctivas en Bucaramanga](#proy4)
5. [Observatorio de Seguridad vial de Bucaramanga](#proy5)
6. [Predictor de ataques cardíacos](#proy6)
7. [Con Los Terroristas (Classifier)](#proy7)
8. [Detección de fraudes en transacciones de tarjetas de crédito](#proy8)
9. [Diagnóstico de diabetes a partir de factores clínicos y hábitos](#proy9)
10. [Recomendador de canciones de spotify](#proy10)
11. [Predicción de delitos en base a factores climáticos en Colombia](#proy11)
12. [Detección de condición de cartas de MTG](#proy12)
13. [Predictor de precios de aguacate en Estados Unidos](#proy13)
14. [Next D'or Man](#proy14)
15. [Pawsitive-Emotions](#proy15)
16. [Clasificador entre células normales o cancerígenas a partir de imágenes microscópicas](#proy16)
17. [Auto-Predict: Machine Learning to Estimate Car Prices](#proy17)
18. [DriveGuide IA](#proy18)
19. [Predicción de precios de Airbnb](#proy19)


---

## Predictor de violencia <a name="proy1"></a>

**Autores: Ricardo Andres López Tarazona, Yefferson Olivos Rodriguez, Levir Heladio Hernandez Suarez Suarez**

<img src="https://github.com/ricardoal94/Violencia/blob/main/banner.jpg?raw=true" style="width:700px;">

**Objetivo:Predecir el tipo de violencia que una persona puede recibir dependiendo de la informacion que nos suministre.**

- Modelo: GaussianNB, DecisionTree, RandomForest y deep learning


[(project sources)](https://github.com/ricardoal94/Violencia)
---



## MODELO DE DETECCION DE ESTADO DE SEMAFOROS PARA AYUDAR A PERSONAS INVIDENTES <a name="proy2"></a>

**Autores: Cesar Enrique Rojas Hernandez - 2191952, Santiago Andres Delgado Quiceno - 211799, Mariana Robayo Nieto - 2195092**

<img src="https://github.com/cesenro2801/PROYECTO_IA/raw/main/IMG/VIDA%20EN%20VERDE.png" style="width:700px;">

**Objetivo:Mejorar la autonomía y seguridad de las personas invidentes al cruzar las calles, mediante el desarrollo de un sistema de IA basado en modelos de aprendizaje que detecten el estado de los semáforos y proporcione indicaciones precisas**

- Modelo: GaussianNB, DecisionTree, RandomForest y deep learning

[(project sources)](https://github.com/cesenro2801/PROYECTO_IA)
---



## EmotionsAI <a name="proy3"></a>

**Autores: Carlos Andrés Gutiérrez Benavides, Juan David Marin Barragán, Daniel Jair Cañate Velasco**

<img src="https://user-images.githubusercontent.com/101914512/254635646-76837a2d-2416-4281-b9ad-3f471dd43158.png" style="width:700px;">

**Objetivo:Detectar tempranamente problemas relacionados a la salud mental.**

- Modelo: Machine Learning (Gaussian Naive Bayes, Decision Tree, Random Forest), Deep Learning (Redes Neuronales), Algoritmos No Supervisados (Agglomerative Clustering, K-Means)

[(project sources)](https://github.com/jdavidmb/EmotionAI/)
---

## Clasificación Multiclase para Medidas Correctivas en Bucaramanga <a name="proy4"></a>

**Autores:  Yeferson Valencia, David Dueñas, Oscar Perez**

<img src="https://github.com/Pepemanpro/ProyectoIA-2023-1/raw/7e5fe5f05b3b137868023fcf0fd4eac5b150f833/Proyecto-IA-2023-1%20Clasificaci%C3%B3n%20Multiclase%20para%20Medidas%20Correctivas%20en%20Bucaramanga.jpg" style="width:700px;">

**Objetivo:desarrollar un modelo de clasificación multiclase que determine las medidas correctivas más apropiadas para las infracciones cometidas en Bucaramanga, utilizando algoritmos de aprendizaje automático y optimización de hiperparámetros.**

- Modelo:Modelos: DecisionTreeClassifier, RandomForestClassifier, GaussianNB, AdaBoostClassifier y HistGradientBoostingClassifier.

[(project sources)](https://github.com/Pepemanpro/ProyectoIA-2023-1/)
---


## Observatorio de Seguridad vial de Bucaramanga <a name="proy5"></a>

**Autores: Rayza Karina**

<img src="https://github.com/raysakarina/inteligencia_artificial/blob/main/Banner%20proyecto%20IA%20Raysa%20Karina%202023-1.png?raw=true" style="width:700px;">

**Objetivo:Predecir elementos de seguridad vial en Bucaramanga**

- Modelo: GaussianNB, DecisionTree, RandomForest y deep learning


[(project sources)](https://github.com/raysakarina/inteligencia_artificial)
---


## Predictor de ataques cardíacos <a name="proy6"></a>

**Autores: Julian David Chacon Camacho, David Alexander Vasquez Vivas, Esneyder Mora Salcedo**

<img src="https://user-images.githubusercontent.com/82472712/255046439-4a797a21-e944-4d16-a90c-848004dd02a4.png" style="width:700px;">

**Objetivo: Predecir la probabilidad de sufrir un ataque cardiaco**

- Modelo: Gaussian Naive Bayes, DecisionTreeClassifier, RandomForestClassifier, SVC, Agglomerative Clustering y K-means


[(project sources)](https://github.com/JulianChacon11/prediccion-ataques-cardiacos)
---


## Con Los Terroristas (Classifier) <a name="proy7"></a>

**Autores: Juan David Escalante Pinilla, Juan David Patiño Pedraza, Daniel Mauricio Pérez Bolívar**

<img src="https://user-images.githubusercontent.com/101756457/255026232-5e6fb54a-bb50-4b84-96fe-55d877f334b3.png" style="width:700px;">

**Objetivo: Utilizar machine learning para detectar ataques y grupos terroristas a través de un clasificador con el fin de centrar esfuerzos y combatir el terrorismo.**

- Modelo: Gaussian Bayes, Decision Tree y Random Forest


[(project sources)](https://github.com/UntetheredJ/AI-Project-Terrorism)
---


## Detección de fraudes en transacciones de tarjetas de crédito <a name="proy8"></a>

**Autores: Ramiro Santiago Avila Chacon, Irvin Aurelio Silva Baez, Cristhian Camilo Rey Rueda**

<img src="https://raw.githubusercontent.com/Irvin-Silva/Proyecto_IA/main/Baner%20IA.png" style="width:700px;">

**Objetivo: Desarrollar un sistema de predicción de transacciones fraudulentas eficiente y preciso.**

- Modelo: Naive Gaussian Bayes, Decision Tree, Random Forest, SVM, Deep Learning, K-Means, DBSCAN y GAs


[(project sources)](https://github.com/Irvin-Silva/Proyecto_IA)
---


## Diagnóstico de diabetes a partir de factores clínicos y hábitos <a name="proy9"></a>

**Autores: Santiago Leal Casanova, Oscar Andres Ramírez Serna, Laura Rodriguez Cala**

<img src="https://raw.githubusercontent.com/Casanova02/Proyecto-IA/main/BannerProyecto.jpg" style="width:700px;">

**Objetivo: Clasificar la existencia de la enfermedad de diabetes a partir de factores clínicos y hábitos de los pacientes usando algoritmos de inteligencia artificial.**

- Modelo: Gaussian Naive Bayes, Decision Tree, Random Forest, Support vector machine (SVM), Red densa


[(project sources)](https://github.com/Casanova02/Proyecto-IA)
---


## Recomendador de canciones de spotify <a name="proy10"></a>

**Autores: Fabian Perez, Paula Arguello, Manuel Herrera**

<img src="https://raw.githubusercontent.com/Factral/spotify-song-recommender/main/images/banner.jpg" style="width:700px;">

**Objetivo: Desarrollar un sistema de recomendación de canciones para Spotify basado en inteligencia artificial que, a partir del análisis de los gustos musicales de un usuario, sea capaz de predecir y sugerir con precisión canciones que se ajusten a sus preferencias personales.**

- Modelo: Dense Neural Networks, Decision Tree.


[(project sources)](https://github.com/Factral/spotify-song-recommender)
---


## Predicción de delitos en base a factores climáticos en Colombia <a name="proy11"></a>

**Autores: Juan Diego Sepulveda Herrera, Juan Pablo Ramirez Ortega, Carlos Arturo Meza Garcia**

<img src="https://user-images.githubusercontent.com/94081346/255046870-d7d12b7a-01ef-4e2c-9acb-a41310dce9e6.jpeg" style="width:700px;">

**Objetivo: Desarrollar un modelo preciso y confiable que utilice datos climáticos para predecir patrones delictivos y proporcionar información valiosa para la prevención y mitigación de crímenes en el país.**

- Modelo: GaussianNaiveBayes, DecisionTreeClassifier, RandomForestClassifier, SupportVectorMachine, DecisionTreeRegressor, RandomForestRegressor, SupportVectorRegression.


[(project sources)](https://github.com/Juand2602/Prediccion_de_delitos_en_base_a_factores_climaticos_en_COL)
---


## Detección de condición de cartas de MTG <a name="proy12"></a>

**Autores: Hendrik López Dueñas**

<img src="https://raw.githubusercontent.com/hendrik21/deteccionCondicionCartasMTG/main/Detecci%C3%B3n%20de%20condici%C3%B3n%20de%20cartas%20de%20MTG.png" style="width:700px;">

**Objetivo: Realizar la detección de la condición de una carta de Magic: The Gathering (MTG), por medio de modelos de clasificación.**

- Modelo: Decission Tree, Random Forest, Support Vector Machine.


[(project sources)](https://github.com/hendrik21/deteccionCondicionCartasMTG)
---


## Predictor de precios de aguacate en Estados Unidos <a name="proy13"></a>

**Autores: Lizeth Ropero, Faiber Angarita, Juan Guerrero.**

<img src="https://user-images.githubusercontent.com/55815692/255089495-cad9836b-7cd5-4312-a255-874087ed40b8.png" style="width:700px;">

**Objetivo: Desarrollar un sistema de IA basado en modelos de aprendizaje que pueda predecir el precio del aguacate.**

- Modelo: Decision Tree Regressor, Random Forest Regressor y Redes neuronales.


[(project sources)](https://github.com/Faiberangarita/IA1_2023-1)
---


## Next D'or Man <a name="proy14"></a>

**Autores: Santiago Andres Chain Santos, Miguel Angel Molina Garzon.**

<img src="https://raw.githubusercontent.com/MiguelAngMolina/Next_D-or_Man/main/Banner.jpeg" style="width:700px;">

**Objetivo: Predecir los próximos ganadores del Balón de Oro basándonos en las estadísticas del videojuego FIFA.**

- Modelo: GaussianNB, RandomForestClassifier, DecisionTreeClassifier, SVC y DecisionTreeRegressor.


[(project sources)](https://github.com/MiguelAngMolina/Next_D-or_Man)
---


## Pawsitive-Emotions <a name="proy15"></a>

**Autores: Juan Camilo mantilla, Alfredo gutierrez nieto, Maximiliano Correa Pico**

<img src="https://user-images.githubusercontent.com/117324114/255193562-cb5162f3-e75f-452e-92f2-1fa87253a2d7.png" style="width:700px;">

**Objetivo: Este proyecto nace con la intención de poder crear una IA que nos permita identificar las emociones que puede estar reflejando una mascota (perro) en su cara.**

- Modelo: GaussianNB, DecisionTreeClassifier, RandomForestClassifier, SVC.


[(project sources)](https://github.com/Maxito06/Proyecto_Final_IA)
---


## Clasificador entre células normales o cancerígenas a partir de imágenes microscópicas <a name="proy16"></a>

**Autores: Juan Bermudez, Jhon Hernández, Mateo Ortiz**

<img src="https://raw.githubusercontent.com/TizMate/Clasificacion-entre-celulas-normales-o-cancerigenas-a-partir-de-imagenes-microscopicas/main/Banner.jpg" style="width:700px;">

**Objetivo: Desarrollar un clasificador de imágenes microscópicas basado en inteligencia artificial que pueda distinguir entre células normales y células cancerígenas leucemicas.**

- Modelo: Gaussian NB, Decision Tree, Random Forest, Support Vector Machine.


[(project sources)](https://github.com/TizMate/Clasificacion-entre-celulas-normales-o-cancerigenas-a-partir-de-imagenes-microscopicas)
---


## Auto-Predict: Machine Learning to Estimate Car Prices <a name="proy17"></a>

**Autores: Jhonnatan David Hernandez Martinez, Diego Andrés Clavijo Granados, Juan Diego Atehortua Duque**

<img src="https://user-images.githubusercontent.com/140187815/255221924-a2e480d0-ac94-4080-95b1-2ffa0648bf1c.jpeg" style="width:700px;">

**Objetivo: Utilizar técnicas de inteligencia artificial para predecir el precio de autos usados, usando características como motor, kilometraje, modelo etc.**

- Modelo: GNB, SVM, DECISSION TREE, RANDOM FOREST, LINEAR REGRESSION.


[(project sources)](https://github.com/Atehortuaduque/autopredict-machine-learning)
---


## DriveGuide IA <a name="proy18"></a>

**Autores: Cristhian Rafael Lizcano Arenas**

<img src="https://gitlab.com/cristhianrafael.lizcano/proyecto-ia/-/raw/main/DriveGuide_IA.png?ref_type=heads" style="width:700px;">

**Objetivo: Identificar carriles para la automatización de los vehiculos.**

- Modelo: Decision tree, Random forest.


[(project sources)](https://gitlab.com/cristhianrafael.lizcano/proyecto-ia)
---


## Predicción de precios de Airbnb <a name="proy19"></a>

**Autores: Cesar Steven Amaya Castellanos, Miguel Santiago Henao Alvarado, Nicolas Torres Barbosa**

<img src="https://raw.githubusercontent.com/Cesar7A/20231-J1-IA/main/Banner.jpg" style="width:700px;">

**Objetivo: Estimar precios posibles para un alquiler de Airbnb basandose en previa informacion de precios, resenas, habitaciones, distancias al metro y cetro civico, capacidad de personas y demas información.**

- Modelo: Decision Tree, Random forest, K-means.


[(project sources)](https://github.com/Cesar7A/20231-J1-IA)
---
