---
# Proyectos 2021-1. Inteligencia Artificial. 

## Prof: Fabio Martínez, Ph.D
## Prof: Gustavo Garzón
---

# Lista de Proyectos
1. [Deteccion de Suicidios](#proy1)
2. [HereComesTheMoney](#proy2)
3. [Clasificación enfermedades oculares](#proy3)
4. [Detección automatizada de malaria: Un enfoque innovador con IA](#proy4)
5. [Clasificación de neumonía en pacientes de pedriatria](#proy5)
6. [Flight Probability](#proy6)
7. [Análisis de sentimiento en Twitter](#proy7)
8. [ZOODEC (Deteccion de objetos, clasificacion de animales)](#proy8)
9. [Captcha Image Solver](#proy9)
10. [Prediccion de Anemia](#proy10)
11. [Analisis en las ventas globales de los videojuegos](#proy11)
12. [Sistema de detección de emociones](#proy12)
13. [Predicción de compra por cliente a partir de cotizaciones de una empresa](#proy13)
14. [An IA approach to algorithmic trading](#proy14)
15. [AI Fashion Discriminator](#proy15)
16. [FALCON EYE](#proy16)
17. [Segmentacion de Plasmodium Falciparum a partir de laminas delgadas de sangre](#proy17)
18. [Detección de trabajos reales o falsos](#proy18)
19. [Prediction of quality of Red Wine](#proy19)
20. [Monkeypox Clasification](#proy20)
21. [Deteccion del grado de acne a traves de imagenes](#proy21)
22. [Predicción: campeón copa mundial](#proy22)
23. [Safetown](#proy23)
24. [Predictor de migración en Colombia](#proy24)


---

## Deteccion de Suicidios <a name="proy1"></a>

**Autores: Daniel Adrian Gonzalez Buendia, Andres Julian Garcia Rueda, Juan Felipe Velandia Naranjo**

<img src="https://github.com/FelipeVelandia/Proyecto-IA/blob/main/BANNER.jpg?raw=true" style="width:700px;">

**Objetivo:Haciendo uso de metodos de Machine Learning, buscamos en base al analisis de publicaciones en redes sociales realizadas por una persona, detectar si tiene tendencias suicidas.**

- Modelo: GaussianNaiveBayes, DecisionTreeClassifier, RandomForestClassifier, SupportVectorMachine, NeuralNetwork



[(code)](https://colab.research.google.com/drive/19p2n8rOOp6vAgfnPygq9wnuFAX3Q5Hhv?usp=share_link) [(video)](https://youtu.be/sthPgAHREcQ) [(+info)](https://github.com/FelipeVelandia/Proyecto-IA)
---

## Here Comes The Money  <a name="proy2"></a>

**Autores: Johan Sebastian Cruz Rueda, Jose Daniel Valera Sánchez, Juan Harvey Quintero Olarte**

<img src="https://camo.githubusercontent.com/a5f521dc41ecee06d3878079ca620ba99825fb90fb13a5357c4a368d3079ed6b/68747470733a2f2f6d656469612e67697068792e636f6d2f6d656469612f764c50726e7556794f5a31596a754f7459412f67697068792d646f776e73697a65642d6c617267652e676966" style="width:700px;">

**Objetivo:desarrollar un proyecto que prediga la tasa de cambio diaria entre el peso colombiano y el dólar estadounidense, con un margen de error mínimo y una alta precisión, utilizando datos históricos del tipo de cambio y factores económicos relevantes, para ello haremos uso de modelos de regresion bastante conocidos**

- Modelo: GaussianNaiveBayes, DecisionTreeClassifier, RandomForestClassifier, SupportVectorMachine, NeuralNetwork
- Dataset del TRM: https://www.kaggle.com/datasets/alonsocopete/trm-dolar-cop-colombia




[(code)](https://github.com/240-Johan/HereComesTheMoney) [(video)]( https:https://youtu.be/pWp5Q4u8hZ0) [(+info)](https://github.com/240-Johan/HereComesTheMoney)
---


## Clasificación enfermedades oculares <a name="proy3"></a>

**Autores: Adel Mauricio Álvarez Amado, Carlos Iván Ramirez Díaz, Kevin Alexis Prada Morales**

<img src="https://github.com/Kevin-STU/Clasificacion-Enfermedades-Oculares/raw/main/banner-ocular.png" style="width:700px;">

**Objetivo:Clasificar las enfermedades oculares como; retinopatía diabética, catarata, glaucoma y normal.**

- Modelo: GaussianNB, DecisionTree, RandomForest, SVM (Poly, sigmoid, rbf) y red neuronal (DNN)
- DAta: El dataset consta de imágenes de glaucoma que utilizamos para balancear el dataset para el problema de "otras vs glaucoma" (sub-problema) https://www.kaggle.com/datasets/sreeharims/glaucoma-dataset


[(code)](https://colab.research.google.com/drive/1Lq67ASNTii6tPVQuGlATg08n1qCDVvMg?usp=sharing) [(video)](https://youtu.be/8RXp65zkVCA) [(+info)](https://github.com/Kevin-STU/Clasificacion-Enfermedades-Oculares)
---



## Detección automatizada de malaria: Un enfoque innovador con IA <a name="proy4"></a>

**Autores: Manuel Alejandro De Angel Fontalvo, Deivyd Dario Parra Perilla, Cristhian Camilo Bustamante Plata.**

<img src="https://gitlab.com/helloimcamilo/detecting_malaria/-/raw/master/banner_ia_project.png" style="width:700px;">

**Objetivo:Desarrollar un sistema de detección de malaria mediante inteligencia artificial, que pueda ser utilizado en condiciones de campo para proporcionar una detección temprana y precisa de la enfermedad.**

- Dataset: En este repositorio, " detecting_malaria/dataset", recuperado de https://www.kaggle.com/datasets/iarunava/cell-images-for-detecting-malaria
- Modelo: GaussianNB, DecisionTree Classifier, Random Forest Classifier, Super Vector Machine, Red neuronal.


[(code)](https://gitlab.com/helloimcamilo/detecting_malaria/-/blob/master) [(video)](https://bit.ly/3E0DqRG) [(+info)](https://gitlab.com/helloimcamilo/detecting_malaria/-/blob/master)
---



## Clasificación de neumonía en pacientes de pedriatria <a name="proy3"></a>

**Autores: Santiago Quintero, Julian León, Brayan Uribe**

<img src="https://user-images.githubusercontent.com/70613830/218635631-22a5c97f-3541-4e1b-a0ac-8a411f967bf3.png" style="width:700px;">

**Objetivo:Poder clasificar imagenes X-RAY de pulmones sanos y enfermos de neumonia.**

- Modelo: Gaussian, Random Forest, Vector Machine, Deep Learning, Decision Tree, K-means


[(code)](https://colab.research.google.com/drive/1625DxkmM8wY7Oxpt-3zyMX6utlZaSZCw?usp=sharing) [(video)](https://www.youtube.com/watch?v=VYAnYU0h890&feature=youtu.be) [(+info)](https://github.com/RobertoOmG777/Proyecto-IA)
---



##  Flight Probability <a name="proy3"></a>

**Autores: Esteban David Florez, Juan Diego Esteban Parra, Jesus Dario Villamizar**

<img src="https://github.com/DiegoEsteban07/Proyecto_Inteligencia_Artificial/raw/main/FLIGHT.2.png" style="width:700px;">

**Objetivo:Crear una Inteligencia Artificial que prediga la cancelación o realizacion de un vuelo comercial mediante clasificacion y deep learning**

- Dataset: nuestro dataset contiene la informacion de todos los vuelos comerciales nacionales e internacionales realizados en los diferentes aeropuertos de Brasil, este dataset lo podremos encontrar en Kaggle: https://www.kaggle.com/datasets/ramirobentes/flights-in-brazil/download?datasetVersionNumber=2
- Modelo: GaussiaNB, DecisionTree, RandomForest y Redes Neuronales



[(code)](https://colab.research.google.com/drive/1qONmCiwXopx_zgAPCUjSRzS5zsiQFhCf) [(video)](https://github.com/DiegoEsteban07/Proyecto_Inteligencia_Artificial/blob/main/video%20Protecto%20IA.mp4) [(+info)](https://github.com/DiegoEsteban07/Proyecto_Inteligencia_Artificial)
---



##  Análisis de sentimiento en Twitter <a name="proy7"></a>

**Autores: Valentina Galeano Castro**

<img src="https://user-images.githubusercontent.com/99009069/219459472-beab7a0d-8c02-4c11-bd70-ab51c5ef70b2.png" width="700">

**Objetivo:Analizar el sentimiento en textos obtenidos de una serie de tweets en un parametro de tiempo con la caracteristica de que se puede establecer cualquier tipo de consulta en la red social.**

- Dataset: Para recopliar los datos se utilizó la técnica de web scraping y se tomaron 500 tweets en la consulta avanzada establecida.
- Modelo: GaussianNB, SVC y Redes Neuronales

[(code)](https://github.com/Vale-gale/TweetsAnalysis/blob/main/cygnusIA.ipynb) [(video)](https://youtu.be/9tp7gooYEls) [(+info)](https://github.com/Vale-gale/TweetsAnalysis)
---



##  ZOODEC (Deteccion de objetos, clasificacion de animales <a name="proy8"></a>

**Autores: Jesus Fernando Ramirez Ardila, Jefersson Alejandro Galeano Hernandez, Juan David Medina Hernández**

<img src="https://raw.githubusercontent.com/jmedinah05/IA/main/Banner.jpeg" width="700">

**Objetivo:Inteligencia artificial la cual es capaz de reconocer distintos tipos de animales a partir de una imagen.**

- Dataset: ?.
- Modelo: GaussianNB, DT, RF y Redes Neuronales

[(code)](https://github.com/jmedinah05/IA/blob/main/Project.ipynb) [(video)](https://www.youtube.com/watch?v=IKhjWf3psH0) [(+info)](https://github.com/jmedinah05/IA)
---



##  Captcha Image Solver <a name="proy9"></a>

**Autores: Javier Andrés Sarmiento Salazar, Felipe Andrés Cepeda Ortiz, Nicolas Velandia Lopez**

<img src="https://raw.githubusercontent.com/Sarmient02/CaptchaImageSolver/main/BannerCaptchaImageSolver.png" width="700">

**Objetivo:Crear una red neuronal capaz de resolver una imagen de tipo Captcha, logrando superar de forma automática estas barreras usadas en muchos sitios web.**

- Dataset: En este repositorio [(Captcha Images - Kaggle)](https://www.kaggle.com/datasets/fanbyprinciple/captcha-images)
- Modelo: Gaussian Naive Bayes, Decision Tree Classifier, Random Forest Classifier, Support Vector Machine y Red Neuronal Convolucional Sequential.

[(code)](https://github.com/Sarmient02/CaptchaImageSolver/blob/main/Captcha_Proyecto_Inteligencia_Artificial.ipynb) [(video)](https://youtu.be/UZdcHM9GJns) [(+info)](https://github.com/Sarmient02/CaptchaImageSolver)
---



##  Prediccion de Anemia <a name="proy10"></a>

**Autores: Dilan Corredor, Sebastian Mendoza, Juan Claro**

<img src="https://camo.githubusercontent.com/92522ff9bca1ecfbc482123dadfdf9ce96fa73c89f7efc5530471b79ea6e87fb/68747470733a2f2f63646e2e646973636f72646170702e636f6d2f6174746163686d656e74732f3833343138303330313339353036363932392f313037353534383830313331353634333433332f42616e6e65725f416e656d69612e6a706567" width="700">

**Objetivo:El objetivo de nuestro proyecto es predecir la anemia en base a los atributos genero, hemoglobina, MCHC, MCUV, MCH, resultados.**

- Dataset: En este repositorio [(Anemia Dataset)](https://www.kaggle.com/datasets/biswaranjanrao/anemia-dataset)
- Modelo: DecisionTreeClassifier, RandomForest, GaussianNB, Support Vector Machine.

[(code)](https://github.com/sebanma/Predicci-n-de-Anemia/blob/main/ProyectoInteligenciaArtificial.ipynb) [(video)](https://www.youtube.com/watch?v=PscUQtIYlfI) [(+info)](https://github.com/sebanma/Predicci-n-de-Anemia.git)
---



##  Analisis en las ventas globales de los videojuegos <a name="proy11"></a>

**Autores: Miguel Daniel Velandia Pinilla, Juan Daniel Suarez Jaimes, Juan Sebastian Espinosa Espinosa**

<img src="https://raw.githubusercontent.com/rokitox05/Proyecto-IA/main/bannerchido.png" width="700">

**Objetivo:El objetivo de nuestro proyecto, es analizar un dataset por medio de la Inteligencia Artificial, y basandonos en algunos criterios, intentar predecir si un videojuego "nuevo" insertado con cierta informacion pueda conseguir buen numero de ventas a nivel global.**

- Dataset: En este repositorio [(Video Game Sales)](https://www.kaggle.com/datasets/gregorut/videogamesales)
- Modelo: GaussianNB, DecisionTree, RandomForest y SVC, y de regresion: DecisionTree y RandomForest.

[(code)](https://github.com/rokitox05/Proyecto-IA/blob/main/IA_Project.ipynb) [(video)](https://www.youtube.com/watch?v=CQnr75VZuog) [(+info)](https://github.com/rokitox05/Proyecto-IA)
---



##  Sistema de detección de emociones <a name="proy12"></a>

**Autores: Guillermo Andres Daza Meneses, Ivan Leonardo Niño Villamil**

<img src="https://raw.githubusercontent.com/IvanLeonardoNino/Proyecto_Final_IA/master/Banner/Banner.jpg" width="700">

**Objetivo:Para este proyecto tenemos como objetivo implementar un modelo de inteligencia artificial capaz de reconocer expresiones faciales, para ello implementamos los conceptos,métodos y planteamientos vistos en clase para obtener un resultado satisfactorio.**

- Dataset: En este repositorio [(Face expression recognition dataset)](https://www.kaggle.com/datasets/jonathanoheix/face-expression-recognition-dataset)
- Modelo: GaussianNB, DT, RF, SVC y red neuronal secuencial

[(code)](https://github.com/IvanLeonardoNino/Proyecto_Final_IA/blob/master/Notebook/Entrega_Final.ipynb) [(video)](https://www.youtube.com/watch?v=eeWbKq9cZSo) [(+info)](https://github.com/IvanLeonardoNino/Proyecto_Final_IA)
---



##  Predicción de compra por cliente a partir de cotizaciones de una empresa <a name="proy13"></a>

**Autores: Miguel Ángel Plata Rodríguez, Adriana Marcela Cujia Reyes, Sebastían Suarez**

<img src="https://raw.githubusercontent.com/Minerisho/Proyecto-AI1-2022-2/main/Banner.jpg" width="700">

**Objetivo: Predecir el valor de la compra de un cliente, a partir de los datos extraídos de múltiples cotizaciones pasadas.**

- Dataset: (propio)
- Modelo: Regression, Random Tree Regressor, Cross Validation, Random Forest Regressor, Support Vector Regressor.

[(code)](https://github.com/Minerisho/Proyecto-AI1-2022-2/blob/main/Proyecto_Final.ipynb) [(video)](https://www.youtube.com/watch?v=Nz2-mNNV38w) [(+info)](https://github.com/Minerisho/Proyecto-AI1-2022-2)
---



##  An IA approach to algorithmic trading <a name="proy14"></a>

**Autores: Daniel Cobos**

<img src="https://raw.githubusercontent.com/danny2768/IAProject/master/ProjectBanner.png" width="700">

**Objetivo: En este proyecto, se aborda el problema de desarrollar un sistema de trading automatizado utilizando técnicas de aprendizaje automático basadas en regresión. El objetivo es crear un modelo que sea capaz de predecir el precio de los activos financieros y tomar decisiones de inversión en tiempo real.**

- Dataset: Los datos se obtendrán de TrueFx - https://www.truefx.com/truefx-historical-downloads/
- Modelo: DTR, RFR y SVR

[(code)](https://github.com/danny2768/IAProject/blob/master/An%20IA%20approach%20to%20algorithmic%20trading.ipynb) [(video)](https://youtu.be/YLg55EvV92o) [(+info)](https://github.com/danny2768/IAProject)
---



##  AI Fashion Discriminator <a name="proy15"></a>

**Autores: Juan Cerinza**

<img src="https://raw.githubusercontent.com/ch0ry/AI-Fashion-Discriminator/master/Images/banner.jpeg" width="700">

**Objetivo: El objetivo de AI Fashion Discriminator es ayudar a las personas a tomar mejores decisiones en el ámbito de la moda, diciéndole al usuario si aquello que viste va a la moda o no.**

- Dataset: (propio)
- Modelo: Se implementó CNN con 3 capas Conv2D, 2 MaxPooling2D, 1 Dense y Output layer. Con un total de 1.127.105 trainable params.

[(code)](https://github.com/ch0ry/AI-Fashion-Discriminator/blob/master/AI%20Fashion%20Discriminator.ipynb) [(video)](https://www.youtube.com/watch?v=RCLCmu3NwjU) [(+info)](https://github.com/ch0ry/AI-Fashion-Discriminator)
---



##  FALCON EYE <a name="proy16"></a>

**Autores: Diego Alejandro López Camacho, Gabriel Fernando Reyes Guevara, Silvia Valerie Guarín Manrique**

<img src="https://raw.githubusercontent.com/diegolopezrm/Falcon-Eye---Reconocimiento-de-voz/main/banner.png" width="700">

**Objetivo: Identificar por medio de una inteligencia artificial los rasgos de la voz para validar, clasificar y controlar, por medio de la edad y género, la autenticidad de un usuario.**

- Dataset: COMMONVOICE - https://commonvoice.mozilla.org/es/datasets
- Modelo: GNB, RF, SVC (rbf, linear, poly), DDN (5 capas)

[(code)](https://github.com/diegolopezrm/Falcon-Eye---Reconocimiento-de-voz/blob/main/Proyecto_AI_Reconocimiento_de_voz.ipynb) [(video)](https://youtu.be/YchcE2CaQbc) [(+info)](https://github.com/diegolopezrm/Falcon-Eye---Reconocimiento-de-voz)
---



##  Segmentacion de Plasmodium Falciparum a partir de laminas delgadas de sangre <a name="proy17"></a>

**Autores: Kevin Santiago Gutierrez Mendez**

<img src="https://raw.githubusercontent.com/SantiagoMendez1/Proyecto-IA-1/main/BannerProyectoIA.png" width="700">

**Objetivo: Implementar metodos de Machine Learning para la segmentacion de parasitos asociados a la malaria en muestras de sangre tomadas a hospedadores del virus.**

- Dataset: "Malaria Segmentation, A dataset for segmentation of malaria infected cells" [(data)](https://www.kaggle.com/datasets/niccha/malaria-segmentation)
- Modelo: Gaussian Naive Bayes, Decision Tree Classifier, Random Forest Classifier

[(code)](https://github.com/SantiagoMendez1/Proyecto-IA-1/blob/main/Proyecto_IA_1_2172020.ipynb) [(video)](https://youtu.be/-beeZPJOaAU) [(+info)](https://github.com/SantiagoMendez1/Proyecto-IA-1)
---



##  Detección de trabajos reales o falsos <a name="proy18"></a>

**Autores: Fabian Sánchez, Carlos Rodriguez**

<img src="https://user-images.githubusercontent.com/83562533/219258891-33ea031a-4202-4833-b511-24cb62d65bc7.jpg" width="700">

**Objetivo: Predecir trabajos fraudulentos o reales, haciendo uso de los estimadores vistos en clase.**

- Dataset: Real or fake jobs [(data)](https://www.kaggle.com/datasets/whenamancodes/real-or-fake-jobs)
- Modelo: GaussianNB, DecisionTreeClassifier, RandomForest, SVC y Deep learning

[(code)](https://github.com/Carlos-CodeBot/IA-Proyecto-DTRF/blob/main/Proyecto_IA_2022_1%20(1).ipynb) [(video)](https://youtu.be/mu8VFl0tT7I) [(+info)](https://github.com/Carlos-CodeBot/IA-Proyecto-DTRF)
---



##  Prediction of quality of Red Wine <a name="proy19"></a>

**Autores: Valery Andrea Melchor Suárez, Julián Andrey Rodríguez Romero, Julián Ricardo Mantilla Castro**

<img src="https://raw.githubusercontent.com/ValeryA26/redwinequality/main/Portada.png" width="700">

**Objetivo: Desarrollar una herramienta capaz de clasificar como buena o mala y calificar con valores entre 0 y 10 la calidad de un vino de acuerdo a sus características fisicoquímicas.**

- Dataset: Red Wine Quality [(data)](https://www.kaggle.com/datasets/uciml/red-wine-quality-cortez-et-al-2009)
- Modelo: GaussianNB, DecisionTree, RandomForest, SVC

[(code)](https://github.com/ValeryA26/redwinequality/blob/main/RedWineQuality.ipynb) [(video)](https://www.youtube.com/watch?v=RXj-ZlqeFBA) [(+info)](https://github.com/ValeryA26/redwinequality)
---



##  Monkeypox Clasification <a name="proy20"></a>

**Autores: César Hurtado, Gysselis Vasquez, Wilmer Farfán**

<img src="https://raw.githubusercontent.com/Churtado26/Monkeypox_Clasification/main/Banner%20IA.png" width="700">

**Objetivo: Desarrollar un modelo de clasificación de imágenes basado en inteligencia artificial capaz de detectar la presencia de viruela del mono en imágenes de distintas partes del cuerpo humano, utilizando técnicas de procesamiento de imágenes y aprendizaje profundo.**

- Dataset: Monkeypox Skin Lesion Dataset [(data)](https://www.kaggle.com/datasets/nafin59/monkeypox-skin-lesion-dataset)
- Modelo: GaussianNB, Decision Tree, Random Forest, Support Vector Machine, DNN, CNN

[(code)](https://github.com/Churtado26/Monkeypox_Clasification/blob/main/Proyecto%20Final%20IA.ipynb) [(video)](https://www.youtube.com/watch?v=O-GTvXt6ghA) [(+info)](https://github.com/Churtado26/Monkeypox_Clasification)
---



##  Deteccion del grado de acne a traves de imagenes <a name="proy21"></a>

**Autores: Laura vanessa mendez rivera, Juan david morantes vergara, Alejandro nuñez herrera**

<img src="https://raw.githubusercontent.com/alenunez/Detenci-n-del-grado-de-acne-a-trav-s-de-imagenes/main/Detecci%C3%B3n%20del%20grado%20de%20acne%20a%20trav%C3%A9s%20de%20imagenes.png" width="700">

**Objetivo: Hallar el nivel del grado de acne de una persona a travez de su foto.**

- Dataset: Acne grading classification dataset [(data)](https://www.kaggle.com/datasets/rutviklathiyateksun/acne-grading-classificationdataset)
- Modelo: GaussianNB, DecisionTreeClassifier, RandomForestClassifier

[(code)](https://github.com/alenunez/Detenci-n-del-grado-de-acne-a-trav-s-de-imagenes/blob/main/Presentacion_1.ipynb) [(video)](https://www.youtube.com/watch?v=0G08SmyNh-8) [(+info)](https://github.com/alenunez/Detenci-n-del-grado-de-acne-a-trav-s-de-imagenes)
---



##  Predicción: campeón copa mundial <a name="proy22"></a>

**Autores: Dilan Corredor, Sebastian Mendoza, Juan Claro**

<img src="https://raw.githubusercontent.com/Fabianernes/proyecto_IA1/30ef83c80144a99eefe817f9265a3e91cd11d697/banner_WWC_2022.jpg" width="700">

**Objetivo: Crear una aproximación de lo que puede ser el resultado de un encuentro de futbol o inclusive conocer cual de los participantes del torneo tiene mas probabilidades de ser el campeón.**

- Dataset: FIFA World Cup 2022 [(data)](https://www.kaggle.com/datasets/brenda89/fifa-world-cup-2022)
- Modelo: GaussianNB, RandomForest, Support Vector Classifier, LogisticRegression, DecisionTree, K-means

[(code)](https://github.com/Fabianernes/proyecto_IA1/blob/main/Winner_World_Cup_2022.ipynb) [(video)](https://www.youtube.com/watch?v=g6OjLzSaEtg) [(+info)](https://github.com/Fabianernes/proyecto_IA1)
---



##  Safetown <a name="proy23"></a>

**Autores: Brajhan Javier Rivera González, Daniel Fernando La Rota Peña, Daniel Ricardo Tavera Camacho**

<img src="https://raw.githubusercontent.com/ricardotavera/Safetown/main/BannerSafeTownBlack.jpg" width="700">

**Objetivo: Model for predicting future crime in the city of Bucaramanga (COL). The model uses a dataset of datos abiertos.gov of all crimes of the city from about last 6 years. Now we are going to predict the location of a posible crime and the kind of weapon to be used. This project uses Random Forest Regressor for making the prediction.**

- Dataset: Delitos en Bucaramanga enero 2010 a diciembre de 2021 [(data)](https://www.datos.gov.co/Seguridad-y-Defensa/40-Delitos-en-Bucaramanga-enero-2010-a-diciembre-d/75fz-q98y)
- Modelo: RandomForestRegressor, DNN, SVR (linear), DesisionTreeRegressor

[(code)](https://github.com/ricardotavera/Safetown/blob/main/DataAnalysis.ipynb) [(video)](https://youtu.be/VIXhzYixBsg) [(+info)](https://github.com/ricardotavera/Safetown)
---



##  Predictor de migración en Colombia <a name="proy24"></a>

**Autores: Jose Carrillo**

<img src="https://raw.githubusercontent.com/JoseAT09/IA-Proyecto/main/Banner.png" width="700">

**Objetivo: Predecir la entrada de migrantes al país utilizando datos registrados de años anteriores.**

- Dataset: Entradas de extranjeros a Colombia [(data)](https://www.datos.gov.co/Estad-sticas-Nacionales/Entradas-de-extranjeros-a-Colombia/96sh-4v8d/data)
- Modelo: Gaussian NB, DecisionTreeClassifier, DecisionTreeRegressor

[(code)](https://github.com/JoseAT09/IA-Proyecto/blob/main/ProyectoFinal.ipynb) [(video)](https://www.youtube.com/watch?v=sYOBVPj8EeM) [(+info)](https://github.com/JoseAT09/IA-Proyecto)
---

