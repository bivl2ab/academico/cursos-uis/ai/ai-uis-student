## Taller abierto de algoritmos genéticos  AI 2024-2

A continuación se presentan los dos puntos  que corresponden al taller 8  de inteligencia artificial y que deben ser desarrollados en el grupo de trabajo que lleva a cabo el proyecto de clase.

Una vez terminados los puntos, envíe el notebook que va adjunto con la solución. En el correo por favor indique los nombres y códigos de los estudiantes del grupo.


### 1. Algoritmos genéticos.


Corra nuevamente el problema del viajero visto en clase, pero esta vez modifique:
La función TSP_cross_over para que retorne  n  hijos. Es decir, un número variable de hijos, de acuerdo a un parámetro de entrada.
La función run_ga para que primero ordene la población de acuerdo a los mejores adaptados y solo realice cruces entre los  K  mejores adaptados.
Corra la simulación y discuta los resultados.

### 2. Algoritmos genéticos - Relación con proyecto


Defina un problema en su proyecto final que pueda ser resuelto utilizando una estrategia de algoritmos genéticos:
- Defina la estrategia para definir un vector de solución.
- Defina una medida de calidad de cada solución.
- Defina una estrategia de crossover.
- Defina una estrategia de mutación.
- Corra la simulación utilizando el notebook de clase. Discuta los resultados.

