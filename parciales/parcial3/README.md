## **Busqeda por contenido: Análisis no supervisado** (30% del tercer parcial)

Para realizar este ejercicio vamos a usar los datos del proyecto del curso.
    - En consecuencia, el ejercicio se podrá trabajar en grupo.



### **Actividades**

1. Realice una partición de los datos en entrenamiento (80 %) y test (20 %)
2. Entrene un algoritmo de k-means (K=20) con el conjunto de entrenamiento.
3. Visualice la disposición de dos características, coloreando el valores de los grupos seleccionados.
4. Realice una función que para cada registro en test (cada fila) retorne el cluster más cercano. Realice una prueba con dos ejemplos.
5. Realice una función que para cada registro en test (cada fila) retorne tres registros (ejemplos de entrenamiento), de forma aleatoria, que también sean del cluster más cercano.
6. Discuta los resultados.
7. haga una visualización de los datos utilizando PCA y tsne


- **UNA VEZ REALIZADO EL NOTEBOOK CON EL INFORME (ACTIVIDADES ANTERIORES) DEBE ENVIARLO AL PROFESOR CORRESPONDIENTE POR CORREO**-->

**UNA VEZ REALIZADO EL NOTEBOOK CON EL INFORME DEBE ENVIARLO AL PROFESOR CORRESPONDIENTE POR CORREO**

- Los correos de los profesores son:
    - Fabio Martínez. famarcar@saber.uis.edu.co
    - Gustavo Garzon.  gustavo.garzon@saber.uis.edu.co

- **FECHA LIMITE: JUEVES 25 DE NOVIEMBRE, 6 PM**
