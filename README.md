# Inteligencia Artificial I 2025-1

## Bienvenidos!

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/ai/ai-uis-student/raw/master/imgs/banner_IA.png"  width="1000px" height="200px">


## Colaboratory (Google)

Vamos a utilizar la plataforma de google para editar, compartir y correr notebooks: [**Colaboratory**](https://colab.research.google.com/notebooks/welcome.ipynb) 

- Necesitas una cuenta de gmail y luego entras a drive
- Colaboratory es un entorno de notebook de Jupyter gratuito que no requiere configuración y se ejecuta completamente en la nube.
    - Usaremos parte de la infraestructura de computo de google... gratis! (máximo procesos de 8 horas)
- Con Colaboratory, puedes escribir y ejecutar código, guardar y compartir análisis, y acceder a recursos informáticos potentes, todo gratis en tu navegador.
- También puedes usar los recursos de computador Local. 


## Calificación
- 30% Talleres
- 10% Talleres en clase. (participaciones por cuartiles)
- 30% Parciales
- 30% Proyecto funcional IA 

## Talleres (Problemsets)

Los talleres pretenden ser una herramienta practica para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada taller esta escrito como un notebook para la validación automática. Se pueden hacer tantos intentos como se quieran y unicamente la última respuesta será tomada en cuenta. Cada uno de los talleres puede ser desarrollado en casa, dentro de las fechas establecidas en el cronograma. 


## Parciales (Quizes)

Son evaluaciones **individuales** basadas en notebooks sobre los temas tratados en las clases. También tendremos un componente teórico, desarrolado en moodle. Los estudiantes deben solucionarlo en el salón de clase, en un tiempo determinado. Los apuntes y notebooks del curso se pueden utilizar (curso del repositorio).

## Proyecto funcional IA

- **Funcionamiento del proyecto**: El proyecto se debe realizar como un notebook y debe ser 100% funcional.

- **Prototipo (PRE-SUS PROJ)**: En este item se considera como esta estructurado el proyecto y se espera una nivel razonable de funcionalidad.

- **Presentación**:
    - Imagen relacionada (800 x 300) con la siguiente información: título del proyecto e información de los estudiantes<br>
    - Video corto (Máximo 5 minutos) (ENTREGAR EL ARCHIVO DE VIDEO y también alojarlo en youtube)<br>
    - Archivo de las diapositivas

- **Sustentación**: Se realizarán preguntas cortas a los estudiantes unicamente relacionadas con el proyecto. 
 
Todos los items tienen el mismo porcentaje de evaluación. 

- **REPOSITORIO DEL PROYECTO**
    - Todos los archivos relacionados con el proyecto deben alojarse en un repositorio de los integrantes del estudainte
    - El archivo readme.md del repositorio debe tener la siguiente información:
        - Titulo del proyecto
        - Banner- Imagen de 800 x 300
        - Autores: Separados por comas
        - Objetivo: Una frase
        - Dataset: información con link de descarga
        - Modelos: Métodos usados para su desarrollo. Escribir solo palabras claves. 
        - Enlaces del código, video, y repositorio
    


**UNICAMENTE SE TENDRAN EN CUENTA LOS PROYECTOS QUE SE HAYAN POSTULADO AL FINALIZAR EL PRIMER CORTE**


## Calendario y plazos

                        SESSION 1            SESSION 2              SESSION SATURDAY


     W01 Feb04-Feb05    Intro                Python-general
     W02 Feb11-Feb12    Python-Numpy         Python-Numpy
     W03 Feb18-Feb19    Genetic Alg          Genetic Alg
     W04 Feb25-Feb26    Pandas               Pandas
     W05 Mar04-Mar05    Visualización        análisis de datos
     W06 Mar11-Mar12    PROYECTO-AVANCE 1    Parcial 1
     W07 Mar18-Mar19    Intro ML-Supervi     Clasificación de una neurona
     W08 Mar25-Mar26    Deep Learning        Deep Learning
     W09 Abr01-Abr02    Otros ML             Evaluación clasificadores
     W10 Abr08-Abr09    ML-Regresion         Evaluación regresores
     W11 Abr15-Abr16    Aplic-imgs           Aplic-audio
     W12 Abr22-Abr23    PROYECTO-AVANCE 2    Parcial 2
     W13 Abr29-Abr30    Intro ML-no sup      ML-no-supervisado
     W14 May06-May07    Evalaución No-sup    Dim red: PCA
     W15 May13-May14    Dim red: tsn-e         supervisado + No supervisado
     W16 May20-May21    PROYECTO-AVANCE 3    Parcial 3
     W17 May27-May28    Sustentaciones       Sustentaciones





     Feb 03            -> Inicio de clases
     Mar 23            -> Fecha limite para reportar primera nota
     Mar 30            -> Ultimo dia para cancelación asignaturas
     Mayo 30            -> Finalización de clases
     jun 03 - jun 06   -> evaluaciones finales
     jun 09            -> Registro calificaciones finales, con actas.
     jun 11            -> habilitaciones
     jun 13            -> Registro de calificaciones definitivas. Solo si hay habilitaciones

    
**ACUERDO n.° acuerdo 278, de 2024**
[Calendario academico](https://uis.edu.co/wp-content/uploads/2024/10/0001_20241008_Acuerdo_278.pdf)




**CUALQUIER ENTREGA FUERA DE PLAZO SERÁ PENALIZADA CON UN 50%**

**LOS PROBLEMSETS ESTAN SUJETOS A CAMBIOS QUE SERÁN DEBIDAMENTE INFORMADOS**

**DEADLINE DE LOS PROBLEMSETS SERÁ EL DIA DE CADA PARCIAL**

